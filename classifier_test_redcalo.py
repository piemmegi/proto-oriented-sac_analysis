# ==========================================================================================
#                                  PREMESSA (sintassi)
# ==========================================================================================
# Questo script analizza i dataset prodotti da "datapreparator.py" ed esegue particle identification
# usando un algorithmo di Machine Learning arbitrario. Questo script serve specificamente a
# fare l'analisi con configurazione ottimale di iperparametri, e ricavare tutti gli score
# da usare per le conclusioni.
#
# Sintassi di call:
#   python classifier_test.py [orientation]
#
# Esempio di call:
# $ conda activate pypmg
# $ cd "C:\Users\Pietro\Desktop\Academic Research\Analisi dati\Analysis_projects\2023\Simulazioni Cristalli Orientati\proto-oriented-sac_analysis"
# $ python classifier_test_redcalo.py am
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import di moduli interni
import matplotlib.pyplot as plt      # Per ambienti di plot
import matplotlib.colors as colors   # Per i colori dei plot
import scipy.io as io                # Per particolari funzioni di I/O
import scipy.signal as sig           # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg         # Per analisi dei segnali e delle immagini
import os                            # Per agire sul sistema operativo
import sys                           # [come sopra]
import numpy as np                   # Per analisi numeriche
import pandas as pd                  # Per analisi numeriche con i dataframe
import time                          # Per il timing degli script
import gc                            # Per la pulizia della memoria
import uproot                        # Per l'upload dei file root
import lmfit                         # Per fit avazati
import json                          # Per leggere i file JSON
import PyPDF2                        # Per unire i PDF alla fine
import glob                          # Per manipolazione dei path
import h5py                          # Per caricare i file .h5
import copy                          # Per copiare gli oggetti in modo corretto
import subprocess                    # Per chiamare script come da bash
import matplotlib as mpl             # Per rendering dei plot
import inspect                       # Per ispezione dei codici
import warnings                      # Per disattivare i Deprecation Warning
import seaborn                       # Per plot aesthetic
from sklearn import preprocessing    # ML / preprocessing
from sklearn import model_selection  # ML / divisione in TrS e TeS
from sklearn import decomposition    # ML / PCA
from sklearn import neighbors        # ML / KNN
from sklearn import metrics          # ML / Metriche di scoring
from sklearn import ensemble         # ML / Random forests

# Import di funzioni singole o funzioni personali, dal file "MyFunctions.py"
sys.path.append("C:\\Users\\Pietro\\Desktop\\Academic Research\\Analisi dati\\Functions\\")
sys.path.append("D:\\Academic Research\\Analisi dati\\Functions\\")
from MyFunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
mpl.rcParams['figure.dpi'] = 300             # Per evitare che i plot inline siamo smearati
warnings.filterwarnings("ignore", category=DeprecationWarning)

# =========================================================================================
#                                      DEFINIZIONI                                      
# =========================================================================================
# Definisco le dimensioni dei font e delle figure e altre variabili generali
textfont = 20         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.png'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filedpi = 540         # Risoluzione in caso di salvataggio di png/jpeg
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                                  DATI DALLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = 'statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
ifVM = statusdata['ifVM']
fileprename = statusdata['fileprename']
n_jobs = statusdata['n_jobs']
random_state = statusdata['random_state']
imgloc = statusdata['imgloc']
temploc = statusdata['temploc']

# ==============================================================================
#                                LOAD DEI DATI
# ==============================================================================
# Definisco il classificatore
classifiertype = 'rf'

# Definisco se sono in amorfo o in assiale
crystalcond = sys.argv[1]

# Definisco il tipo di fascio
beamtype = 'unif_g251_unif_h501'

# Definisco se devo usare Ein o Etot come reference
ifEin = False

# Apro il file dati e lo spacchetto in dataset + codifica
t0 = time.perf_counter()
print(f"Loading file...")

filename = fileprename + 'eqmerged_data_' + beamtype + '.npz'
with np.load(filename) as dataset:
    # Creo un dataframe a partire dal dataset
    df = pd.DataFrame(dataset['data_' + crystalcond],dtype=np.float32)

    print(f"Elapsed time (for data loading and dataframe conversion): {(time.perf_counter()-t0):.2f} s \n")

    # Converto il dataframe in un 2D-array, per renderlo fruibile per analisi
    # future (il dataset originale era un 1D array di tuple...)
    X = np.array(df.iloc[:,1:])    # Ogni riga è un pattern, ogni colonna una feature
    nrows,ncols = np.shape(X)      # Dimensioni del dataset

    # Estraggo il vettore delle classi e lo converto da categorical a numerical
    y = np.int16(df.iloc[:,0])  
        
    # Mi segno come avviene la conversione
    lut = '1 = gamma, 0 = neutron\n'
    print(f"LUT: {lut}")
    numclasses = 2

    # Estraggo il vettore di binning in energie
    Ebins = dataset['Ebins']
    tEbins = dataset['tEbins']

# Rigenero il dataset rimuovendo le informazioni dei layer 3 e 4
newX_1 = X[:,:50] # energie nelle torri dei primi due layer
newX_2a = X[:,-5]*X[:,-1] # energie nei primi due layer
newX_2b = X[:,-4]*X[:,-1] # energie nei primi due layer
newX_3 = newX_2a+newX_2b
newX = np.zeros((np.shape(newX_1)[0], np.shape(newX_1)[1]+3))
newX[:,:-3] = newX_1
newX[:,-3] = newX_2a / newX_3
newX[:,-2] = newX_2b / newX_3
newX[:,-1] = newX_3

# Taglio in un intervallo specifico
Emin = 26
Emax = 51 # con 51 GeV garantisco almeno 8k eventi/bin ma non metto cap (fotoni >> neutroni)
Ebinstep = 2.5
cond = (newX_3 >= Emin) & (newX_3 < Emax)
X = newX[cond,:]
y = y[cond]
Ebins = np.arange(Emin,Emax+Ebinstep,Ebinstep)
tEbins = truebins(Ebins)
NumBins = len(tEbins)

print(f"Dataset shape: {X.shape}")

# ==============================================================================
#                                HYPERPARAMETERS
# ==============================================================================
# Determino gli iper-parametri da applicare al classificatore
gs_bestpar = {'n_estimators': 200,
              'criterion': 'gini',
              'max_depth': None,
              'max_features': 15}
print("\n")

# Definisco altri iperparametri globali per il classificatore e la grid search
n_splits = 5                                   # TrS / TeS splitting (= K nella K-Fold crossvalidation)
kf = model_selection.KFold(n_splits=n_splits)
PCAtype = 'standard'                           # Che PCA usare ("incremental" se Incrementale, standard altrimenti)
thr = 0.95                                     # Cut sulla mCEV per la selezione delle componenti principali
nev = None                                     # Quanti eventi del dataset (= #TrS + #TeS) uso? None = tutti
nrocstep = 100000                              # Quanti punti per calcolare la curva ROC
roc_grid = np.linspace(0,1,nrocstep)           # Valori di FPR(x) per il calcolo della curva ROC
print(f"Working with: {n_jobs} core(s) \n")

# ==============================================================================
#                           OPERATION OF THE CLASSIFIER
# ==============================================================================
# Definisco il nome del file di output e non procedo se esso esiste già
filename = os.path.join(temploc,f'opt__results__redcalo__{crystalcond}__{classifiertype}__{beamtype}.npz')
if (os.path.exists(filename)) & False:
    print(f"The following output file was found:")
    print(filename)
    print(f"Skipping the testing... \n")
else:
    # Itero: a ogni passo splitto il dataset in TrS/TeS, preparo i dati, addestro il 
    # classificatore e poi lo uso.
    classifier_accuracy = np.zeros((NumBins,n_splits))
    classifier_precision = np.zeros_like(classifier_accuracy)
    classifier_recall = np.zeros_like(classifier_accuracy)
    classifier_confusion = np.zeros((numclasses,numclasses,NumBins,n_splits))

    classifier_roc_global = np.zeros((nrocstep,NumBins,n_splits))
    classifier_auc_global = np.zeros((NumBins,n_splits))

    classifier_prcurve_global = np.zeros_like(classifier_roc_global)
    classifier_prauc_global = np.zeros_like(classifier_auc_global)

    t0 = time.perf_counter()

    for i, (train_index, test_index) in enumerate(kf.split(X)):
        # ==============================================================================
        #                                   DATA SPLITTING
        # ==============================================================================
        # Suddivido il dataset in TrS e TeS
        print(f"Step {i+1} / {n_splits} of the K-Fold validation")
        X_train = X[train_index,:]
        y_train = y[train_index]
        X_test = X[test_index,:]
        y_test = y[test_index]

        # Estraggo l'energia da usare come reference. Può essere Ein o Etot in base
        # al tipo di file
        X_test_energies = X_test[:,-1]

        # ==============================================================================
        #                                   DATA PREPARATION
        # ==============================================================================
        # Definisco il costruttore per lo scaling standard
        print(f"Entering the preprocessing phase: scaling, PCA and pruning...")
        scaler = preprocessing.StandardScaler()
        scaler.fit(X_train)
        X_train_scaled = scaler.transform(X_train)

        # Definisco il costruttore per la PCA
        if (PCAtype == 'incremental'):
            pca = decomposition.IncrementalPCA()
        else:
            pca = decomposition.PCA()
        pca.fit(X_train_scaled)
        X_train_pca = pca.transform(X_train_scaled)

        # Calcolo quante componenti principali tenere
        X_ev = pca.explained_variance_ratio_
        X_cumev = np.cumsum(X_ev)
        n_underthr = np.where(X_cumev<=thr)[0].shape[0]
        print(f"Number of Principal Components after the pruning: {n_underthr} / {np.shape(X_cumev)[0]}")

        # Estraggo X e Y dei dati del TrS dopo scaling e PCA
        X_train_pruned = X_train_pca[:,:n_underthr]

        # Estraggo X e Y dei dati del TeS dopo scaling e PCA    
        X_test_scaled = scaler.transform(X_test)
        X_test_pca = pca.transform(X_test_scaled)
        X_test_pruned = X_test_pca[:,:n_underthr]
        print(f"Preprocessing ended!")
        
        # ==============================================================================
        #                                   TRAINING
        # ==============================================================================
        # Definisco il costruttore del classificatore
        print(f"Beginning the training phase...")
        # Creo e addestro il classificatore
        Classifier = ClassifierConstructor(classifiertype,gs_bestpar,random_state,n_jobs)
        Classifier.fit(X_train_pruned,y_train)
        print(f"Training completed!")

        # ==============================================================================
        #                                   TESTING
        # ==============================================================================
        # Itero su ogni bin energetico
        for j,elj in enumerate(tEbins):
            # Estraggo la parte di TeS che rispetta il taglio energetico
            cond = (X_test_energies >= Ebins[j]) & (X_test_energies < Ebins[j+1])
            X_test_cutted = X_test_pruned[cond,:]
            y_test_cutted = y_test[cond]

            # Predico le classi del TeS e le probabilità di classificazione
            y_test_predicted = Classifier.predict(X_test_cutted)
            y_test_predicted_proba = Classifier.predict_proba(X_test_cutted)

            # ==============================================================================
            #                                 SCORE
            # ==============================================================================
            # Scoring
            classifier_accuracy[j,i] = metrics.accuracy_score(y_test_cutted,y_test_predicted)
            classifier_precision[j,i] = metrics.precision_score(y_test_cutted,y_test_predicted)
            classifier_recall[j,i] = metrics.recall_score(y_test_cutted,y_test_predicted)
            
            # Costruisco la matrice di confusione
            classifier_confusion[:,:,j,i] = metrics.confusion_matrix(y_test_cutted,y_test_predicted)
            
            # ==============================================================================
            #                               ROC CURVE
            # ==============================================================================
            # Costruisco la curva ROC.
            fpr, tpr, _ = metrics.roc_curve(y_test_cutted,y_test_predicted_proba[:,1])
            
            # Dal momento che a ogni K-Fold potrei avere un diverso insieme di threshold,
            # non posso semplicemente salvare a ogni passo fpr e tpr e poi mediare.
            # Piuttosto, devo interpolare su una grid predefinita.
            classifier_roc_global[:,j,i] = np.interp(roc_grid, fpr, tpr)
            classifier_auc_global[j,i] = metrics.auc(fpr, tpr)

            # ==============================================================================
            #                         PRECISION-RECALL CURVE
            # ==============================================================================
            # Costruisco la curva PR.
            prec, rec, _ = metrics.precision_recall_curve(y_test_cutted,y_test_predicted_proba[:,1])
            
            # Dal momento che a ogni K-Fold potrei avere un diverso insieme di threshold,
            # non posso semplicemente salvare a ogni passo prec e rec e poi mediare.
            # Piuttosto, devo interpolare su una grid predefinita. 
            #
            # Note su "precision_recall_curve" (che la differenziano da "roc_curve"):
            #   1. E' restituita prima la precisione poi la recall, cioè prima y e poi x della 
            #       curva PR. Quindi vanno scambiare nell'interpolazione.
            #   2. Precision e recall sono ordinate in modo che la recall sia monotona non
            #       crescente. Questo significa che nell'interpolazione bisogna invertire
            #       i vettori (da coda, cioè elemento -1, a testa, cioè elemento 0).
            classifier_prcurve_global[:,j,i] = np.interp(roc_grid, rec[::-1], prec[::-1])
            classifier_prauc_global[j,i] = metrics.average_precision_score(y_test_cutted,y_test_predicted_proba[:,1])

        # Chiudo
        print(f"Testing completed!\n\n")

    # Salvo gli scoring medi in questa configurazione (= a questo passo di GS) e le loro incertezze
    av_classifier_accuracy = np.mean(classifier_accuracy,axis=1)
    av_classifier_precision = np.mean(classifier_precision,axis=1)
    av_classifier_recall = np.mean(classifier_recall,axis=1)

    std_classifier_accuracy = np.std(classifier_accuracy,axis=1)/np.sqrt(n_splits-1)
    std_classifier_precision = np.std(classifier_precision,axis=1)/np.sqrt(n_splits-1)
    std_classifier_recall = np.std(classifier_recall,axis=1)/np.sqrt(n_splits-1)

    # Salvo la matrice di confusione media
    av_classifier_confusion = np.mean(classifier_confusion,axis=3)
    std_classifier_confusion = np.std(classifier_confusion,axis=3)/np.sqrt(n_splits-1)

    # Salvo le curve ROC medie e le rispettive AUC
    av_roc_global = np.mean(classifier_roc_global,axis=2)
    std_roc_global = np.std(classifier_roc_global,axis=2)/np.sqrt(n_splits-1)
    av_auc_global = np.mean(classifier_auc_global,axis=1)
    std_auc_global = np.std(classifier_auc_global,axis=1)/np.sqrt(n_splits-1)

    # Salvo le curve PR medie e le rispettive AUC
    av_prcurve_global = np.mean(classifier_prcurve_global,axis=2)
    std_prcurve_global = np.std(classifier_prcurve_global,axis=2)/np.sqrt(n_splits-1)
    av_prauc_global = np.mean(classifier_prauc_global,axis=1)
    std_prauc_global = np.std(classifier_prauc_global,axis=1)/np.sqrt(n_splits-1)

    # Stampo il timing corrente
    tend = time.perf_counter()
    print(f"Time elapsed since the beginning of the K-Fold: {(tend-t0):.3f} s\n")

    # Salvo tutti gli output di ricerca in npz
    np.savez_compressed(filename, 
            av_classifier_accuracy = av_classifier_accuracy,       # Accuracy media
            av_classifier_precision = av_classifier_precision,     # Precisione media
            av_classifier_recall = av_classifier_recall,           # Recall media
            av_classifier_confusion = av_classifier_confusion,     # Confusion matrix media
            av_roc_global = av_roc_global,                         # Curva ROC media
            av_auc_global = av_auc_global,                         # AUC media
            av_prcurve_global = av_prcurve_global,                 # Curva PR media
            av_prauc_global = av_prauc_global,                     # AUC della PR media
            std_classifier_accuracy = std_classifier_accuracy,     # Errore sull'accuracy media
            std_classifier_precision = std_classifier_precision,   # Errore sulla precisione media
            std_classifier_recall = std_classifier_recall,         # Errore sulla recall media
            std_classifier_confusion = av_classifier_confusion,    # Errore sulla confusion matrix media
            std_roc_global = std_roc_global,                       # Errore sulla curva ROC media
            std_auc_global = std_auc_global,                       # Errore sulla AUC media
            std_prcurve_global = std_prcurve_global,               # Errore sulla curva PR media
            std_prauc_global = std_prauc_global,                   # Errore sulla AUC della PR media
            roc_grid = roc_grid,                                   # Grid di punti (asse x curva ROC)
            Ebins = Ebins,                                         # Estremi dei bin energetici
            tEbins = tEbins,                                       # Centri dei bin energetici
            NumBins = NumBins)                                     # Numero di bin energetici
            
    # Salvo tutti gli output di ricerca in npz
    print(f"Job done! The output files were written at: ")
    print(filename)

# Concludo
print(f"")