# ==========================================================================================
#                                  PREMESSA (sintassi)
# ==========================================================================================
# Questo script acquisisce in input quattro file dati raw generati via Geant4, contenenti
# i risultati delle simulazioni eseguite con due tipo di particelle diverse (fotoni, neutroni)
# e in due condizioni diverse (random, asse). Lo script estrae gli eventi e produce alcune
# mappe di correlazione, utili per comprendere meccanismi manuali di classificazione.
# Di fatti, si correlano Etot con EL0 (raw o normalizzata) e con EL1 (raw o normalizzata).
# Si possono anche applicare tagli su Ein, se disponibile.
#
# Sintassi di call:
#   	python pubplots_ELXvsEdep_scatter.py [ifnorma] [ifEcut]
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt        # Per ambienti di plot
import matplotlib.colors as colors     # Per i colori dei plot
import scipy.io as io                  # Per particolari funzioni di I/O
import scipy.signal as sig             # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg           # Per analisi dei segnali e delle immagini
import os                              # Per agire sul sistema operativo
import sys                             # [come sopra]
import numpy as np                     # Per analisi numeriche
import pandas as pd                    # Per analisi numeriche con i dataframe
import time                            # Per il timing degli script
import gc                              # Per la pulizia della memoria
import uproot                          # Per l'upload dei file root
import lmfit                           # Per fit avazati
import json                            # Per leggere i file JSON
import PyPDF2                          # Per unire i PDF alla fine
import glob                            # Per manipolazione dei path
import h5py                            # Per caricare i file .h5
import copy                            # Per copiare gli oggetti in modo corretto
import subprocess                      # Per chiamare script come da bash
import matplotlib as mpl               # Per rendering dei plot
from sklearn import utils as sk_utils  # Per AI

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
sys.path.append("/eos/user/p/pmontigu/Functions/")
sys.path.append("C:\\Users\\Pietro\\Desktop\\Academic Research\\Analisi dati\\Functions\\")
sys.path.append("D:\\Academic Research\\Analisi dati\\Functions\\")
from MyFunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
mpl.rcParams['figure.dpi'] = 300             # Per evitare che i plot inline siamo smearati

# =========================================================================================
#                                      DEFINIZIONI                                      
# =========================================================================================
# Definisco le dimensioni dei font e delle figure e altre variabili generali
textfont = 20         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filedpi = 320         # Risoluzione in caso di salvataggio di png/jpeg
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                                  DATI DALLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = 'statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Estraggo tutte le variabili di interesse dal file di status
ifVM = statusdata['ifVM']
fileprename = statusdata['fileprename']
n_jobs = statusdata['n_jobs']
imgloc = statusdata['imgloc']
temploc = statusdata['temploc']

# ================================================================================
#                                 VARIABILI GENERALI
# ================================================================================
# Definisco il fascio da usare
#beamtype = 'unif_g251_unif_h501'         # Physics run at equal Edep
beamtype = 'unif_g201_unif_h201'         # Physics run at equal Ein
#beamtype = 'unif1_251unif1_251large'      # Old run

# Definisco le condizioni di cristallo da analizzare
crystalconds = ['ax','am']
all_titles = ['Random alignment','Axial alignment']           # Questi sono i titoli dei plot!

# Deduco dal tipo di fascio se in questo file è salvata anche l'energia iniziale (Ein)
if (beamtype == 'unif_g251_unif_h501') | (beamtype == 'unif1_251unif1_251large'):
	ifEin = False
elif (beamtype == 'unif_g201_unif_h201'):
	ifEin = True

# Definisco quanti eventi tenere prima di ulteriori tagli
nmaxevents = 10000

# Definisco se normalizzate gli assi verticali dei plot o no
try:
	ifnorma = (sys.argv[1] == 'True')
except:
	ifnorma = False

# Dimensione dei marker nei plot
dimsize = 10

# Definisco se applicare un taglio anche su Ein (o Edep) oppure no.
# N.B. il taglio su Ein si fa se e solo se essa esiste!
try:
	ifEcut = (sys.argv[2] == 'True')
except:
	ifEcut = False

# Definisco i parametri per l'eventuale taglio su Ein
if ifEin:
	Ein_mean = 10      # GeV, seleziono chi è in [mean-width,mean+width]
	Ein_width = 1      # GeV
else:
	Ein_mean = 120     # GeV, seleziono chi è in [mean-width,mean+width]
	Ein_width = 1      # GeV

# Definisco gli indici di colonna dove si trovano le energie di layer.
# N.B. questi sono gli stessi per i dataset con e senza Ein, perché tanto
# le variabili "data_a*_*" sono costruite prendendo solo le energie dei 
# layer e l'energia totale
ind_EL0 = 0
ind_EL1 = 1
ind_EL2 = 2
ind_EL3 = 3
ind_Etot = -1

# ==========================================================================================
#                                      DEFINIZIONE COLORI
# ==========================================================================================
# Costruisco colori per gli scatter plot che siano BW compliant
gnuplot_cmap = plt.get_cmap('gnuplot2')
indices = np.linspace(0, gnuplot_cmap.N, 20)
my_colors = [gnuplot_cmap(int(i)) for i in indices]
crystalcondcolors = [my_colors[4],my_colors[13]] # Blu, Rosso

# ================================================================================
#                                 ESTRAZIONE DATI
# ================================================================================
# Apro i file per entrambe le condizioni e ne estraggo i dati
outputdata = []
outputclasses = []
if ifEin:
	outputEin = []

forcedEmin = 26             # Cut inferiore in energia

print(f"")
print(f"Job starting in configuration: ifnorma = {ifnorma}, ifEcut = {ifEcut}")
print(f"Beginning the data extraction...\n")

for i,el in enumerate(crystalconds):
	# Definisco il nome del file dati
	filename = fileprename + 'eqmerged_data_' + beamtype + '.npz'
	with np.load(filename) as dataset:
	    # Creo un dataframe a partire dal dataset
	    df = pd.DataFrame(dataset['data_' + el],dtype=np.float32)

	    # Creo la condizione di taglio in energia: devo decidere se totale o incidente
	    if ifEin:
	    	Etobecut = np.array(df.iloc[:,-6:])
	    else:
	    	Etobecut = np.array(df.iloc[:,-1])

	    cutcond = (Etobecut >= forcedEmin)


	    # Estraggo le energie parziali e totali dal dataframe
	    partial_energies = np.array(df.iloc[cutcond,-5:])

	    # Se le energie parziali sono normalizzate, le moltplico per Edep
	    # in modo da poterne usare il valore in GeV in questo script
	    ifDataAreNormalized = dataset['ifnorma']
	    if ifDataAreNormalized:
	    	for j in range(4):
	    		partial_energies[:,j] = partial_energies[:,j] * partial_energies[:,-1]
	    
	    outputdata.append(partial_energies)

	    # Se esistono, estraggo le energie iniziali
	    if ifEin:
	    	outputEin.append(np.array(df.iloc[cutcond,-6]))

	    # Estraggo le classi
	    y = np.int16(df.iloc[cutcond,0])
	    outputclasses.append(y)

print(f"Extraction done!\n")

# ================================================================================
#                                 SELEZIONE DATI
# ================================================================================
# Separo i dati in amorfo e assiale, gamma e neutrone secondo la codifica di tutti
# i file: g = 1, n = 0
cond_ax_gamma = (outputclasses[0] > 0)
cond_ax_neutron = (cond_ax_gamma == False)
cond_am_gamma = (outputclasses[1] > 0)
cond_am_neutron = (cond_am_gamma == False)

data_ax_gamma = outputdata[0][cond_ax_gamma,:]
data_ax_neutron = outputdata[0][cond_ax_neutron,:]
data_am_gamma = outputdata[1][cond_am_gamma,:]
data_am_neutron = outputdata[1][cond_am_neutron,:]

# Se esiste, estraggo Ein
if ifEin:
	Ein_ax_gamma = outputEin[0][cond_ax_gamma]
	Ein_ax_neutron = outputEin[0][cond_ax_neutron]
	Ein_am_gamma = outputEin[1][cond_am_gamma]
	Ein_am_neutron = outputEin[1][cond_am_neutron]

# ================================================================================
#                                CUT IN ENERGIA INIZIALE
# ================================================================================
# Se sto guardando un dataset costruito inserendo anche l'energia iniziale dei
# delle particelle incidenti, posso fare un taglio sul suo valore
if ifEcut:
	# Costruisco il taglio
	if ifEin:
		# Taglio su Ein
		cond_ax_gamma   = (Ein_ax_gamma >= Ein_mean - Ein_width) & \
						  (Ein_ax_gamma <= Ein_mean + Ein_width)
		cond_ax_neutron = (Ein_ax_neutron >= Ein_mean - Ein_width) & \
						  (Ein_ax_neutron <= Ein_mean + Ein_width)
		cond_am_gamma   = (Ein_am_gamma >= Ein_mean - Ein_width) & \
						  (Ein_am_gamma <= Ein_mean + Ein_width)
		cond_am_neutron = (Ein_am_neutron >= Ein_mean - Ein_width) & \
						  (Ein_am_neutron <= Ein_mean + Ein_width)
	else:
		# Taglio su Etot
		cond_ax_gamma   = (data_ax_gamma[:,ind_Etot] >= Ein_mean - Ein_width) & \
						  (data_ax_gamma[:,ind_Etot] <= Ein_mean + Ein_width)
		cond_ax_neutron = (data_ax_neutron[:,ind_Etot] >= Ein_mean - Ein_width) & \
						  (data_ax_neutron[:,ind_Etot] <= Ein_mean + Ein_width)
		cond_am_gamma   = (data_am_gamma[:,ind_Etot] >= Ein_mean - Ein_width) & \
						  (data_am_gamma[:,ind_Etot] <= Ein_mean + Ein_width)
		cond_am_neutron = (data_am_neutron[:,ind_Etot] >= Ein_mean - Ein_width) & \
						  (data_am_neutron[:,ind_Etot] <= Ein_mean + Ein_width)

	# Lo applico
	data_ax_gamma = data_ax_gamma[cond_ax_gamma,:]
	data_ax_neutron = data_ax_neutron[cond_ax_neutron,:]
	data_am_gamma = data_am_gamma[cond_am_gamma,:]
	data_am_neutron = data_am_neutron[cond_am_neutron,:]

# Indipendentemente dal taglio, compatto i dati in due liste per rendere 
# più facile l'iterazione su di esse
data_gamma = [data_am_gamma,data_ax_gamma]
data_neutron = [data_am_neutron,data_ax_neutron]
if ifEin:
	if ifEcut:
		Ein_am_gamma = Ein_am_gamma[cond_am_gamma]
		Ein_ax_gamma = Ein_ax_gamma[cond_ax_gamma]
		Ein_am_neutron = Ein_am_neutron[cond_am_neutron]
		Ein_ax_neutron = Ein_ax_neutron[cond_ax_neutron]

	Ein_gamma = [Ein_am_gamma,Ein_ax_gamma]
	Ein_neutron = [Ein_am_neutron,Ein_ax_neutron]

# ================================================================================
#                                SCATTER Etot vs EL0
# ================================================================================
# Produco un plot di correlazione 2D di energia depositata: Totale vs L0
fig = plt.figure(figsize = dimfig)
for i in range(2):
	# Calcolo la normalizzazione dell'asse verticale. Può essere un vettore di 1,
	# se non si vuole normalizzare, o l'energia depositata in totale.
	if ifnorma:
		normavec_gamma = data_gamma[i][:,ind_Etot]/100
		normavec_neutron = data_neutron[i][:,ind_Etot]/100
		ylims = [0,60]
		ystring = 'Fraction of energy deposited in L0 [%]'
	else:
		normavec_gamma = np.ones_like(data_gamma[i][:,ind_Etot])
		normavec_neutron = np.ones_like(data_neutron[i][:,ind_Etot])
		ylims = [0,30]
		ystring = 'Energy deposited in L0 [GeV]'

	# Produco il plot
	ax = fig.add_subplot(1,2,i+1)
	ax.scatter(data_gamma[i][:nmaxevents,ind_Etot],data_gamma[i][:nmaxevents,ind_EL0] / normavec_gamma[:nmaxevents],
							color=crystalcondcolors[0],marker='.',s=dimsize,label=r'$\gamma$')
	ax.scatter(data_neutron[i][:nmaxevents,ind_Etot],data_neutron[i][:nmaxevents,ind_EL0] / normavec_neutron[:nmaxevents],
							color=crystalcondcolors[1],marker='.',s=dimsize,label=r'$n$')
	ax.set_xlabel('Total deposited energy [GeV]',fontsize = textfont)
	ax.set_ylabel(ystring,fontsize = textfont)
	ax.set_ylim(ylims)
	leg = ax.legend(loc='upper left',fontsize=textfont,framealpha=1)
	leg.legend_handles[0]._sizes = [100]
	leg.legend_handles[1]._sizes = [100]
	ax.grid(linewidth=0.5)
	ax.set_title(all_titles[i],fontsize=textfont)

fig.set_tight_layout('tight')
filename = f"pubplots_ifEcut_{ifEcut}_ifnorma_{ifnorma}_scatter_EL0vsEdep" + filetype
fig.savefig(os.path.join(imgloc,f"{beamtype}",filename),dpi=filedpi)
#plt.show()
plt.close(fig)

print(f"Scatterplot Etot vs EL0 (normalization: {ifnorma}): done!\n")

# ================================================================================
#                                SCATTER Etot vs EL1
# ================================================================================
# Produco un plot di correlazione 2D di energia depositata: Totale vs L1
fig = plt.figure(figsize = dimfig)
for i in range(2):
	# Calcolo la normalizzazione dell'asse verticale. Può essere un vettore di 1,
	# se non si vuole normalizzare, o l'energia depositata in totale.
	if ifnorma:
		normavec_gamma = data_gamma[i][:,ind_Etot]/100
		normavec_neutron = data_neutron[i][:,ind_Etot]/100
		ylims = [0,80]
		ystring = r'$E_{L,2} / E_{dep}$ [%]'
	else:
		normavec_gamma = np.ones_like(data_gamma[i][:,ind_Etot])
		normavec_neutron = np.ones_like(data_neutron[i][:,ind_Etot])
		ylims = [0,60]
		ystring = 'Energy deposited in L1 [GeV]'

	# Costruisco l'average trend
	histo2D_gamma, binsX_gamma, binsY_gamma = truehisto2D(data_gamma[i][:,ind_Etot],data_gamma[i][:,ind_EL1] / normavec_gamma,
														  nbinsX = 30, nbinsY = 200)

	truex_gamma, histosum_gamma, histosum_err_gamma = histoproj(histo2D_gamma,binsX_gamma,binsY_gamma,
														  axis='x',projtype='mean',errtype='Taylor')

	histo2D_neutron, binsX_neutron, binsY_neutron = truehisto2D(data_neutron[i][:,ind_Etot],data_neutron[i][:,ind_EL1] / normavec_neutron,
														  nbinsX = 30, nbinsY = 200)

	truex_neutron, histosum_neutron, histosum_err_neutron = histoproj(histo2D_neutron,binsX_neutron,binsY_neutron,
														  axis='x',projtype='mean',errtype='Taylor')

	# Produco il plot
	ax = fig.add_subplot(1,2,i+1)
	ax.scatter(data_gamma[i][:nmaxevents,ind_Etot],data_gamma[i][:nmaxevents,ind_EL1] / normavec_gamma[:nmaxevents],
							color=crystalcondcolors[0],marker='.',s=dimsize,label=r'$\gamma$',
							rasterized=True)
	ax.scatter(data_neutron[i][:nmaxevents,ind_Etot],data_neutron[i][:nmaxevents,ind_EL1] / normavec_neutron[:nmaxevents],
							color=crystalcondcolors[1],marker='.',s=dimsize,label=r'$n$',
							rasterized=True)
	ax.plot(truex_gamma, histosum_gamma, color='cyan',label=r'$\bar{\gamma}$',
								linewidth=1.5,linestyle='-',marker='^',markersize=12.5,
								markeredgecolor='black',markeredgewidth=1.5)
	ax.plot(truex_neutron, histosum_neutron, color='yellow',label=r'$\bar{n}$',
							linewidth=1.5,linestyle='-',marker='X',markersize=12.5,
							markeredgecolor='black',markeredgewidth=1.5)
	ax.set_xlabel(r"Deposited energy (E$_{dep}$) [GeV]",fontsize = textfont)
	ax.set_ylabel(ystring,fontsize = textfont*0.9)
	ax.set_ylim(ylims)
	leg = ax.legend(loc='upper right',fontsize=textfont,framealpha=1)
	leg.legend_handles[0]._sizes = [100]
	leg.legend_handles[1]._sizes = [100]
	ax.grid(linewidth=0.5)
	ax.set_title(all_titles[i],fontsize=textfont)
	ax.tick_params(labelsize = textfont*0.75)

fig.set_tight_layout('tight')
filename = f"pubplots_ifEcut_{ifEcut}_ifnorma_{ifnorma}_scatter_EL1vsEdep" + filetype
fig.savefig(os.path.join(imgloc,f"{beamtype}",filename),dpi=filedpi)
#plt.show()
plt.close(fig)

print(f"Scatterplot Etot vs EL1 (normalization: {ifnorma}): done!\n")

# ================================================================================
#                                SCATTER Ein vs EL1
# ================================================================================
# Produco un plot di correlazione 2D di energia depositata: Ein vs L1 (** se Ein esiste)
if ifEin:
	fig = plt.figure(figsize = dimfig)
	for i in range(2):
		# Calcolo la normalizzazione dell'asse verticale. Può essere un vettore di 1,
		# se non si vuole normalizzare, o l'energia depositata in totale.
		if ifnorma:
			normavec_gamma = Ein_gamma[i]/100
			normavec_neutron = Ein_neutron[i]/100
			ylims = [0,70]
			ystring = r'$E_{L,2} / E_{in}$ [%]'
			thisloc = 'upper right'
		else:
			normavec_gamma = np.ones_like(data_gamma[i][:,ind_Etot])
			normavec_neutron = np.ones_like(data_neutron[i][:,ind_Etot])
			ylims = [0,60]
			ystring = r'$E_{L,2}$ [GeV]'
			thisloc = 'upper left'

		# Costruisco l'average trend
		histo2D_gamma, binsX_gamma, binsY_gamma = truehisto2D(Ein_gamma[i],data_gamma[i][:,ind_EL1] / normavec_gamma,
															  nbinsX = 30, nbinsY = 200)

		truex_gamma, histosum_gamma, histosum_err_gamma = histoproj(histo2D_gamma,binsX_gamma,binsY_gamma,
															  axis='x',projtype='mean',errtype='Taylor')

		histo2D_neutron, binsX_neutron, binsY_neutron = truehisto2D(Ein_neutron[i],data_neutron[i][:,ind_EL1] / normavec_neutron,
															  nbinsX = 30, nbinsY = 200)

		truex_neutron, histosum_neutron, histosum_err_neutron = histoproj(histo2D_neutron,binsX_neutron,binsY_neutron,
															  axis='x',projtype='mean',errtype='Taylor')

		# Produco il plot
		ax = fig.add_subplot(1,2,i+1)
		ax.scatter(Ein_gamma[i][:nmaxevents],data_gamma[i][:nmaxevents,ind_EL1] / normavec_gamma[:nmaxevents],
								color=crystalcondcolors[0],marker='.',s=dimsize,label=r'$\gamma$',
								rasterized=True)
		ax.scatter(Ein_neutron[i][:nmaxevents],data_neutron[i][:nmaxevents,ind_EL1] / normavec_neutron[:nmaxevents],
								color=crystalcondcolors[1],marker='.',s=dimsize,label=r'$n$',
								rasterized=True)
		ax.plot(truex_gamma, histosum_gamma, color='cyan',label=r'$\bar{\gamma}$',
								linewidth=1.5,linestyle='-',marker='^',markersize=12.5,
								markeredgecolor='black',markeredgewidth=1.5)
		ax.plot(truex_neutron, histosum_neutron, color='yellow',label=r'$\bar{n}$',
								linewidth=1.5,linestyle='-',marker='X',markersize=12.5,
								markeredgecolor='black',markeredgewidth=1.5)
		ax.set_xlabel(r'Initial energy ($E_{in}$) [GeV]',fontsize = 1.25*textfont)
		ax.set_ylabel(ystring,fontsize = 1.25*textfont)
		ax.set_ylim(ylims)
		leg = ax.legend(loc=thisloc,fontsize=textfont,framealpha=1)
		leg.legend_handles[0]._sizes = [100]
		leg.legend_handles[1]._sizes = [100]
		ax.grid(linewidth=0.5)
		ax.set_title(all_titles[i],fontsize=textfont*1.5)
		ax.tick_params(labelsize = textfont*0.75)

	fig.set_tight_layout('tight')
	filename = f"pubplots_ifEcut_{ifEcut}_ifnorma_{ifnorma}_scatter_EL1vsEin" + filetype
	fig.savefig(os.path.join(imgloc,f"{beamtype}",filename),dpi=filedpi)
	#plt.show()
	plt.close(fig)

	print(f"Scatterplot Ein vs EL1 (normalization: {ifnorma}): done!\n")

# ================================================================================
#                                SCATTER Ein vs Etot
# ================================================================================
# Produco un plot di correlazione 2D di energia depositata: Ein vs Etot (** se Ein esiste)
ifcompute = True 
thrcompute = 50     # Compute how many photons are > this thr, if (ifcompute == True)

if ifEin:
	fig = plt.figure(figsize = dimfig)
	for i in range(2):
		# Calcolo la normalizzazione dei dati. Può essere un vettore di 1,
		# se non si vuole normalizzare, o l'energia depositata in totale.
		if ifnorma:
			normavec_gamma = Ein_gamma[i][:nmaxevents]/100
			normavec_neutron = Ein_neutron[i][:nmaxevents]/100
			ystring = r'$E_{dep} / E_{in}$ [%]'
			xlims = [26,150]
			ylims = [0,125]
			thisloc = 'upper right'

			# Thresholding
			if ifcompute:
				# Calcolo su tutti gli eventi la frazione di energia iniziale
				# depositata nel calorimetro
				gammaFracEdep = 100*data_gamma[i][:,ind_Etot] / Ein_gamma[i]
				neutronFracEdep = 100*data_neutron[i][:,ind_Etot] / Ein_neutron[i]

				# Calcolo quanti eventi discrimino correttamente con un thresholding
				# semplice
				numgamma_overthr = np.sum((gammaFracEdep >= thrcompute))
				numneutron_overthr = np.sum((neutronFracEdep <= thrcompute))

				# Calcolo la corrispondente accuratezza di classificazione
				numgamma_all = np.shape(gammaFracEdep)[0]
				numneutron_all = np.shape(neutronFracEdep)[0]
				proto_accuracy = (numgamma_overthr + numneutron_overthr) / (numgamma_all + numneutron_all)
				print(f"Case: Ein vs Edep, in condition {all_titles[i]} with threshold {thrcompute}")
				print(f"Proto-accuracy: {proto_accuracy*100:.2f} %\n")
		else:
			normavec_gamma = np.ones_like(data_gamma[i][:nmaxevents,ind_Etot])
			normavec_neutron = np.ones_like(data_neutron[i][:nmaxevents,ind_Etot])
			ystring = r'Deposited energy (E$_{dep}$) [GeV]'
			xlims = [0,150]
			ylims = [0,150]
			thisloc = 'upper left'

		# Produco il plot
		ax = fig.add_subplot(1,2,i+1)
		ax.scatter(Ein_gamma[i][:nmaxevents],data_gamma[i][:nmaxevents,ind_Etot] / normavec_gamma,
								color=crystalcondcolors[0],marker='.',s=dimsize,label=r'$\gamma$',
								rasterized=True)
		ax.scatter(Ein_neutron[i][:nmaxevents],data_neutron[i][:nmaxevents,ind_Etot] / normavec_neutron,
								color=crystalcondcolors[1],marker='.',s=dimsize,label=r'$n$',
								rasterized=True)
		if ifcompute:
			ax.plot(xlims,[thrcompute,thrcompute],color='black',linestyle='--',linewidth=5,
								label = f'{thrcompute} %')
		ax.set_xlabel(r'Initial energy (E$_{in}$) [GeV]',fontsize = 1.25*textfont)
		ax.set_ylabel(ystring,fontsize = 1.25*textfont)
		leg = ax.legend(loc=thisloc,fontsize=textfont,framealpha=1)
		leg.legend_handles[0]._sizes = [100]
		leg.legend_handles[1]._sizes = [100]
		ax.grid(linewidth=0.5)
		ax.set_title(all_titles[i],fontsize=textfont*1.5)
		ax.set_xlim(xlims)
		ax.set_ylim(ylims)
		ax.tick_params(labelsize = textfont*0.75)

	fig.set_tight_layout('tight')
	filename = f"pubplots_ifEcut_{ifEcut}_ifnorma_{ifnorma}_scatter_EdepvsEin" + filetype
	fig.savefig(os.path.join(imgloc,f"{beamtype}",filename),dpi=filedpi)
	#plt.show()
	plt.close(fig)

	print(f"Scatterplot Ein vs Edep (normalization: {ifnorma}): done!\n")

# ================================================================================
#                                HISTO1D of EL0
# ================================================================================
# Produco un istogramma di energia depositata in L0
ifdo = False
nbins = 100

if ifdo:
	fig = plt.figure(figsize = dimfig)
	for i in range(2):
		# Calcolo la normalizzazione dei dati. Può essere un vettore di 1,
		# se non si vuole normalizzare, o l'energia depositata in totale.
		if ifnorma:
			normavec_gamma = data_gamma[i][:,ind_Etot]/100
			normavec_neutron = data_neutron[i][:,ind_Etot]/100
			xlims = [0,70]
			xstring = 'Fraction of energy deposited in L0 [%]'
		else:
			normavec_gamma = np.ones_like(data_gamma[i][:,ind_Etot])
			normavec_neutron = np.ones_like(data_neutron[i][:,ind_Etot])
			xlims = [0,40]
			xstring = 'Energy deposited in L0 [GeV]'

		# Produco gli istogrammi e li normalizzo
		Ebins = np.linspace(xlims[0],xlims[-1],nbins)
		tEbins = truebins(Ebins)

		histo_gamma, _ = truehisto1D(data_gamma[i][:,ind_EL0] / normavec_gamma, bins=Ebins)
		histo_neutron, _ = truehisto1D(data_neutron[i][:,ind_EL0] / normavec_neutron, bins=Ebins)

		histo_gamma = histo_gamma / np.sum(histo_gamma)
		histo_neutron = histo_neutron / np.sum(histo_neutron)

		# Produco il plot
		ax = fig.add_subplot(1,2,i+1)
		histoplotter1D(ax,tEbins,histo_gamma,leglabel = r'$\gamma$', hcolor = crystalcondcolors[0])
		histoplotter1D(ax,tEbins,histo_neutron,leglabel = r'$n$', hcolor = crystalcondcolors[1])
		ax.set_xlabel(xstring,fontsize = textfont)
		ax.set_ylabel('Normalized counts',fontsize = textfont)
		ax.set_yscale('log')
		ax.legend(loc='best',fontsize=textfont,framealpha=1)
		ax.grid(linewidth=0.5)
		ax.set_title(all_titles[i],fontsize=textfont)

	fig.set_tight_layout('tight')
	filename = f"pubplots_ifEcut_{ifEcut}_ifnorma_{ifnorma}_histo1D_EL0" + filetype
	fig.savefig(os.path.join(imgloc,f"{beamtype}",filename),dpi=filedpi)
	#plt.show()
	plt.close(fig)

	print(f"Histo1D EL0 (normalization: {ifnorma}): done!\n")
else:
	print(f"Skipping the histo1D EL0... \n")

# ================================================================================
#                                HISTO1D of EL1
# ================================================================================
# Produco un istogramma di energia depositata in L1
nbins = 100
ifdo = False

if ifdo:
	fig = plt.figure(figsize = dimfig)
	for i in range(2):
		# Calcolo la normalizzazione dei dati. Può essere un vettore di 1,
		# se non si vuole normalizzare, o l'energia depositata in totale.
		if ifnorma:
			normavec_gamma = data_gamma[i][:,ind_Etot]/100
			normavec_neutron = data_neutron[i][:,ind_Etot]/100
			xlims = [0,75]
			xstring = 'Fraction of energy deposited in L1 [%]'
		else:
			normavec_gamma = np.ones_like(data_gamma[i][:,ind_Etot])
			normavec_neutron = np.ones_like(data_neutron[i][:,ind_Etot])
			xlims = [0,70]
			xstring = 'Energy deposited in L1 [GeV]'

		# Produco gli istogrammi e li normalizzo
		Ebins = np.linspace(xlims[0],xlims[-1],nbins)
		tEbins = truebins(Ebins)

		histo_gamma, _ = truehisto1D(data_gamma[i][:,ind_EL1] / normavec_gamma, bins=Ebins)
		histo_neutron, _ = truehisto1D(data_neutron[i][:,ind_EL1] / normavec_neutron, bins=Ebins)

		histo_gamma = histo_gamma / np.sum(histo_gamma)
		histo_neutron = histo_neutron / np.sum(histo_neutron)

		# Produco il plot
		ax = fig.add_subplot(1,2,i+1)
		histoplotter1D(ax,tEbins,histo_gamma,leglabel = r'$\gamma$', hcolor = crystalcondcolors[0])
		histoplotter1D(ax,tEbins,histo_neutron,leglabel = r'$n$', hcolor = crystalcondcolors[1])
		ax.set_xlabel(xstring,fontsize = textfont)
		ax.set_ylabel('Normalized counts',fontsize = textfont)
		ax.set_yscale('log')
		ax.legend(loc='best',fontsize=textfont,framealpha=1)
		ax.grid(linewidth=0.5)
		ax.set_title(all_titles[i],fontsize=textfont)

	fig.set_tight_layout('tight')
	filename = f"pubplots_ifEcut_{ifEcut}_ifnorma_{ifnorma}_histo1D_EL1" + filetype
	fig.savefig(os.path.join(imgloc,f"{beamtype}",filename),dpi=filedpi)
	#plt.show()
	plt.close(fig)

	print(f"Histo1D EL1 (normalization: {ifnorma}): done!\n")
else:
	print(f"Skipping the histo1D EL1... \n")

# ================================================================================
#                                HISTO1D of Etot
# ================================================================================
# Produco un istogramma di energia depositata in totale
nbins = 100
ifdo = False

if ifdo:
	fig = plt.figure(figsize = dimfig)
	for i in range(2):
		# Produco gli istogrammi e li normalizzo
		xlims = [0,151]
		Ebins = np.linspace(xlims[0],xlims[-1],nbins)
		tEbins = truebins(Ebins)

		histo_gamma, _ = truehisto1D(data_gamma[i][:,ind_Etot], bins=Ebins)
		histo_neutron, _ = truehisto1D(data_neutron[i][:,ind_Etot], bins=Ebins)

		histo_gamma = histo_gamma / np.sum(histo_gamma)
		histo_neutron = histo_neutron / np.sum(histo_neutron)

		# Produco il plot
		ax = fig.add_subplot(1,2,i+1)
		histoplotter1D(ax,tEbins,histo_gamma,leglabel = r'$\gamma$', hcolor = crystalcondcolors[0])
		histoplotter1D(ax,tEbins,histo_neutron,leglabel = r'$n$', hcolor = crystalcondcolors[1])
		ax.set_xlabel('Total energy deposited [GeV]',fontsize = textfont)
		ax.set_ylabel('Normalized counts',fontsize = textfont)
		ax.set_yscale('log')
		ax.legend(loc='best',fontsize=textfont,framealpha=1)
		ax.grid(linewidth=0.5)
		ax.set_title(all_titles[i],fontsize=textfont)

	fig.set_tight_layout('tight')
	filename = f"pubplots_ifEcut_{ifEcut}_ifnorma_{ifnorma}_histo1D_Etot" + filetype
	fig.savefig(os.path.join(imgloc,f"{beamtype}",filename),dpi=filedpi)
	#plt.show()
	plt.close(fig)

	print(f"Histo1D Etot: done!\n")
else:
	print(f"Skipping the histo1D Etot... \n")

print(f"Job done!\n")