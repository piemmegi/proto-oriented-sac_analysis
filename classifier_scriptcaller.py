# ==========================================================================================
#                                  PREMESSA (sintassi)
# ==========================================================================================
# Questo script chiama in successione gli script per la pipeline di analisi dati con Machine
# Learning:
#   - Grid Search (classifier_gridsearch, classifier_gridsearchreport)
#   - Test in configurazione ottimale (classifier_test)
#   - Rappresentazione dei risultati (classifier_finalreport, classifier_finalreport_PR)
#
# Sintassi di call:
#   python classifier_scriptcaller.py [beamtype] [classifier]
#
# Esempio di call:
# $ conda activate pypmg
# $ cd "C:\Users\Pietro\Desktop\Academic Research\Analisi dati\Analysis_projects\2023\Simulazioni Cristalli Orientati\proto-oriented-sac_analysis"
# $ python classifier_scriptcaller.py unif_g251_unif_h501 rf
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import di moduli interni
import os                            # Per agire sul sistema operativo
import sys                           # [come sopra]
import time                          # Per il timing degli script
import gc                            # Per la pulizia della memoria
import uproot                        # Per l'upload dei file root
import json                          # Per leggere i file JSON
import glob                          # Per manipolazione dei path
import copy                          # Per copiare gli oggetti in modo corretto
import subprocess                    # Per chiamare script come da bash
import inspect                       # Per ispezione dei codici

# Import di funzioni singole o funzioni personali, dal file "MyFunctions.py"
sys.path.append("/eos/user/p/pmontigu/Functions/")
sys.path.append("C:\\Users\\Pietro\\Desktop\\Academic Research\\Analisi dati\\Functions\\")
sys.path.append("D:\\Academic Research\\Analisi dati\\Functions\\")
from MyFunctions import *

# ==============================================================================
#                  DEFINIZIONE INPUT PER GLI SCRIPT SUCCESSIVI
# ==============================================================================
# In che condizioni guardo il cristallo?
crystalconds = ['ax','am']

# Che fascio (file dati) uso? Ammessi: "unif_g251_unif_h501", "unif_g201_unif_h201""
beamtype = sys.argv[1] 

# Che classificatore uso? Ammessi: "rf" (Random Forest), "nn" (Neural Network), ...
classifier = sys.argv[2] 
if (classifier == "rf"):
    print(f"Classifier chosen: Random Forest")
elif (classifier == "nn"):
    print(f"Classifier chosen: Neural Network")
else:
    print(f"X"*50)
    print(f"Classifier chosen: {classifier} ---> not recognized! Falling back on ''rf'' option...")
    print(f"X"*50)
    classifier = "rf"

# Definisco il numero di iperparametri disponibili per ogni classificatore
if (classifier == 'rf'):
    num_hp = 4
elif (classifier == 'nn'):
    num_hp = 6

# ==============================================================================
#                                   MASTERCALL
# ==============================================================================
# Definisco una funzione che gestisca gli split tra uno script e l'altro
def DoneWithScript(donestring = f"Job done!\n\n"):
    print(donestring)
    for _ in range(3):
        print(50*'*')

    print(f"")

# Printout di controllo
print(f"\n" + f"Job starting" + "\n")
    
# Processo i dataset in amorfo e axial
for i,el in enumerate(crystalconds):
    # Chiamo lo script per eseguire una GS con il classificatore prescelto e 
    # ottenere la configurazione ottimale per la classificazione gamma-neutrone
    print(f"{el} configuration")
    print(f"Calling classifier_gridsearch.py ...\n")
    subprocess.call(['python', 'classifier_gridsearch.py', classifier, el, beamtype])
    DoneWithScript()

    # Analizzo i risultati della grid search con uno script apposito
    print(f"Calling classifier_gridsearchreport.py for each hyperparameters combination ...")
    for j in range(num_hp):
        for k in range(num_hp-j-1):
            subprocess.call(['python', 'classifier_gridsearchreport.py', classifier, el, beamtype, f"{j}", f"{j+k+1}"])
    subprocess.call(['python', 'classifier_gridsearchreport.py', classifier, el, beamtype, f"{3}", f"{0}"])
    DoneWithScript()

    # Chiamo lo script per addestrare il classificatore prescelto con la configurazione
    # ottimale di iperparametri, per misurarne l'accuracy di classificazione in funzione
    # dell'energia depositata nel calorimetro.
    print(f"Calling classifier_test.py ...\n")
    subprocess.call(['python', 'classifier_test.py', classifier, el, beamtype])
    DoneWithScript()

# Produco gli output grafici di sintesi del lavoro
print(f"Calling classifier_finalreport.py ...\n")
subprocess.call(['python', 'classifier_finalreport.py', classifier, beamtype])
DoneWithScript()

print(f"Calling classifier_finalreport_PR.py ...\n")
subprocess.call(['python', 'classifier_finalreport_PR.py', classifier, beamtype])
DoneWithScript()

print(f"All jobs done! \n\n")