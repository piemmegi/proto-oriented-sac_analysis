# ==========================================================================================
#                                  PREMESSA (sintassi)
# ==========================================================================================
# Questo script acquisisce in input quattro file dati raw generati via Geant4, contenenti
# i risultati delle simulazioni eseguite con due tipo di particelle diverse (fotoni, neutroni)
# e in due condizioni diverse (random, asse). Lo script estrae gli eventi e salva:
# 	- La loro classe
# 	- L'energia di ogni cristallo (L0_00, L0_01, ..., L3_44)
#	- [Se presente] L'energia iniziale della particella
# 	- L'energia di ogni layer (L0, L1, L2, L3)
# 	- L'energia totale
# Nel file prodotto in output si tiene solo un numero fissato di eventi totale 
# (per bin energetico e per tipo di particella). La sintassi dei file è quella
# dell'elenco: classe, EL*_**, Ein (se presente), EL*, Etot
#
# Sintassi di call:
#   python datapreparator.py [beamtype]
#
# Esempio di call:
# $ conda activate pypmg
# $ cd "C:\Users\Pietro\Desktop\Academic Research\Analisi dati\Analysis_projects\2023\Simulazioni Cristalli Orientati\proto-oriented-sac_analysis"
# $ python datapreparator_redcalo.py
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt        # Per ambienti di plot
import matplotlib.colors as colors     # Per i colori dei plot
import scipy.io as io                  # Per particolari funzioni di I/O
import scipy.signal as sig             # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg           # Per analisi dei segnali e delle immagini
import os                              # Per agire sul sistema operativo
import sys                             # [come sopra]
import numpy as np                     # Per analisi numeriche
import pandas as pd                    # Per analisi numeriche con i dataframe
import time                            # Per il timing degli script
import gc                              # Per la pulizia della memoria
import uproot                          # Per l'upload dei file root
import lmfit                           # Per fit avazati
import json                            # Per leggere i file JSON
import PyPDF2                          # Per unire i PDF alla fine
import glob                            # Per manipolazione dei path
import h5py                            # Per caricare i file .h5
import copy                            # Per copiare gli oggetti in modo corretto
import subprocess                      # Per chiamare script come da bash
import matplotlib as mpl               # Per rendering dei plot
from sklearn import utils as sk_utils  # Per AI

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
sys.path.append("/eos/user/p/pmontigu/Functions/")
sys.path.append("C:\\Users\\Pietro\\Desktop\\Academic Research\\Analisi dati\\Functions\\")
sys.path.append("D:\\Academic Research\\Analisi dati\\Functions\\")
from MyFunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
mpl.rcParams['figure.dpi'] = 300             # Per evitare che i plot inline siamo smearati

# =========================================================================================
#                                      DEFINIZIONI                                      
# =========================================================================================
# Definisco le dimensioni dei font e delle figure e altre variabili generali
textfont = 20         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.png'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filedpi = 540         # Risoluzione in caso di salvataggio di png/jpeg
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                                  DATI DALLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = 'statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Estraggo tutte le variabili di interesse dal file di status
ifVM = statusdata['ifVM']
fileprename = statusdata['fileprename']
n_jobs = statusdata['n_jobs']
imgloc = statusdata['imgloc']
temploc = statusdata['temploc']

# ================================================================================
#              INFORMAZIONI PRELIMINARI SULL'ESTRAZIONE DATI
# ================================================================================
# Definisco il fascio da usare
beamtype == 'unif_g251_unif_h501'

# Definisco le configurazioni esistenti
crystalconfig = ['axial','amorphous']
particles = ['gamma','neutron']
crystalcolors = ['mediumblue','red']

# Definisco la codifica di neutroni e fotoni
idvalues = [1,0]   # Assegno 0 ai neutroni e 1 ai fotoni

# Quante variabili raw esistono nei file dati?
nvars = 50         # 2 layer da 5x5 cristalli

# Quante variabili aggiungere oltre a quelle raw?
extravars = 3       # 2 energie per layer, 1 energia totale

# Nel nome delle key dei dati salvati, qual è l'indice (della stringa)
# che indica il layer di appartenenza?
layerid = 10

# Normalizzo l'energia depositata per layer?
ifnorma = True

# ================================================================================
#              INFORMAZIONI PRELIMINARI SULL'APERTURA FILE
# ================================================================================
# Apro tutti i file in una volta o in bunch di eventi?
# N.B. il numero di eventi per bunch deve essere un sottomultiplo del numero di eventi
# in ogni run... (e.g., se faccio run da 250k eventi, ok bunch da 50k o 125k, no ok 100k)
NumEventsPerBunch = 50000 # Quanti eventi per bunch
NumBunchesForPrint = 10

# ================================================================================
#              INFORMAZIONI PRELIMINARI SULLA SELEZIONE EVENTI
# ================================================================================
# Vettore di binning per gli spettri energetici
Emin = 26        # GeV
Emax = 51      # GeV
Ebinstep = 2.5  # GeV
Ebins = np.arange(Emin,Emax+Ebinstep,Ebinstep)
tEbins = truebins(Ebins)
NumBins = len(tEbins)

# Quanti eventi voglio estrarre, per ogni bin e per ogni tipo di particella?
NumEventsPerBin = 7500

# ================================================================================
#							ESTRAZIONE DEI DATI DAL FILE
# ================================================================================
# Itero su tutte le combinazioni particella/cristallo.
outputdata = []
print(f"Beginning the data extraction...\n")

for i,eli in enumerate(crystalconfig):
	for j,elj in enumerate(particles):
		# Alloco lo slot per i dati prodotti in questa configurazione
		outputdata.append([])
		
		# Definisco un vettore che contiene il numero totale di eventi già estratti
		# nei vari bin energetici. Lo dovrò aggiornare ad ogni bunching step.
		NumEventsExtracted = np.zeros_like(tEbins)
		
		# ================================================================================
		#                            ESTRAZIONE DATI
		# ================================================================================
		# Definisco il nome del file da caricare
		filepostname = 'tbeamdata_' + eli + '_' + elj + '_' + beamtype + '.root'
		print(f"Crystal in {eli} alignment, {elj} beam")
		print(f"Filename: {filepostname}")
		
		# Apro il file e calcolo il numero di eventi a disposizione
		inputfile = uproot.open(fileprename + filepostname)['outData']
		nevents = inputfile['NEvent'].num_entries
		print(f"Eventi disponibili: {nevents}")

		# ================================================================================
		#                            BUNCHING
		# ================================================================================
		# Itero su ogni bunch di dati
		numbunches = int(np.ceil(nevents/NumEventsPerBunch))

		for bunch_ind in range(numbunches):
			if (bunch_ind % NumBunchesForPrint == 0):
				print(f"Bunch n. {bunch_ind+1} / {numbunches}")

			# Preinizializzo le matrici da riempire con i dati del file:
			# 	- "midoutputdata" contiene le energie depositate in ogni cristallo
			#		più l'energia dei layer, l'energia totale e la classe (**e Ein,
			#		se esistente)
			#	- "partial_energies" contiene solo l'energia dei layer e l'energia totale
			# Colonne: nvars (energie layer) + extravars (layer e totale) + 1 (classe)
			midoutputdata = np.zeros((NumEventsPerBunch,nvars+extravars+1),dtype=np.float32)
			partial_energies = np.zeros((NumEventsPerBunch,extravars),dtype=np.float32)
			
			# Estraggo le energie depositate in ogni cristallo evento per evento.
			# I dati estratti finiscono in una matrice che ha sei colonne extra: 
			#	- Classe (fotoni = 1, neutroni = 0)
			#	- Frazione di energia depositata in ogni layer in z (integrando in x e y)
			# 	  normalizzata all'energia totale
			#	- Energia totale depositata nel calorimetro
			
			for k,elk in enumerate(inputfile.keys()):
				# Estraggo e salvo l'energia depositata in un singolo cristallino.
				# Se sto guardando la key "numero di evento", inserisco al suo posto 
				# l'identificatore di particella, mentre salto i seed.
				if elk == 'NEvent':
					midoutputdata[:,k] = idvalues[j]
				elif (elk == 'Seed0') | (elk == 'Seed1') | (elk[layerid] == '2') | (elk[layerid] == '3'):
					continue
				elif (elk == 'TotalEnergyOfPrimaryParticle'):
					Ein = np.array(inputfile[elk].array(entry_start = NumEventsPerBunch*bunch_ind,
														entry_stop = NumEventsPerBunch*(bunch_ind+1)),
														dtype = np.float32)
				else:
					# Estraggo l'energia del layer.
					energy_crys = np.array(inputfile[elk].array(entry_start = NumEventsPerBunch*bunch_ind,
																entry_stop = NumEventsPerBunch*(bunch_ind+1)),
																dtype = np.float32)

					# Salvo le energie nella colonna corretta
					midoutputdata[:,k] = energy_crys
					
					# Aggiorno le energie parziali di ogni layer
					layerind = int(elk[layerid])
					partial_energies[:,layerind] += energy_crys   # Aumento l'energia del layer parziale
					partial_energies[:,-1] += energy_crys         # Aumento l'energia totale

			# Normalizzo le energie depositate per layer al totale, se richiesto
			if ifnorma:
				# Applico la normalizzazione
				for normind in range(extravars-1):
					partial_energies[:,normind] = partial_energies[:,normind] / partial_energies[:,-1]

				# Correggo eventuali casi di 0/0 che portano a NaN
				partial_energies[np.isnan(partial_energies)] = 0


			# A fine ciclo, inserisco le energie parziali (ed eventualmente Ein)
			# nel dataset totale
			midoutputdata[:,-extravars:] = partial_energies
			
			# Clear temporaneo di memoria
			del energy_crys, partial_energies
			gc_cleaner()
			
			# ================================================================================
			#                            SELEZIONE DEGLI EVENTI
			# ================================================================================
			# Itero su tutti i bin energetici. Per ogni bin, seleziono un numero fissato di
			# eventi da estrarre
			outputdata_ij = np.zeros((0,nvars+extravars+1))
			
			for m,elm in enumerate(tEbins):
				# Verifico se il bin è già stato riempito completamente: in caso, salto
				# N.B. questo if non sarebbe strettamente necessario, ma risparmia l'esecuzione
				# di alcuni step non necessari...
				if NumEventsExtracted[m] >= NumEventsPerBin:
					continue

				# Estraggo tutti gli eventi dove ho un'energia nel range indicato dal bin scelto.
				# Se Ein è disponibile, taglio su di lui
				cond = (midoutputdata[:,-1] >= Ebins[m]) & (midoutputdata[:,-1] < Ebins[m+1])
				subset_data = midoutputdata[cond,:]
				
				# Determino quanti di questi eventi vanno salvati
				NumEventsAvailableThisBin = np.shape(subset_data)[0]
				NumEventsMaxToBeSaved = NumEventsPerBin - NumEventsExtracted[m]
				NumEventsToBeSaved = int(np.min(np.array( [NumEventsAvailableThisBin, NumEventsMaxToBeSaved ])))
				
				# Estraggo gli eventi per il salvataggio
				outputdata_ij = np.vstack((outputdata_ij,subset_data[:NumEventsToBeSaved,:]))
				
				# Aumento il counter di eventi estratti
				NumEventsExtracted[m] += NumEventsToBeSaved

			# Salvo l'output intermedio nella lista di output finale, nel corretto elemento
			outputdata[-1].append(outputdata_ij)

		# ================================================================================
		#                                 CLEAR FINALI
		# ================================================================================
		# Mostro se manca statistica
		print(f"*"*15)
		print(f"Missing events: {np.sum(NumEventsPerBin-NumEventsExtracted)}")
		print(f"*"*15)

		# Chiudo il file a fine iterazione e aggiorno il counter per i plot
		inputfile.close()
		print(f"")

		# Data deletion and garbage collection
		del midoutputdata, outputdata_ij
		gc_cleaner()
		print(f"Done!\n")

print(f"Extraction done!\n\n")

# Al termine di tutta l'estrazione, devo unire i dati prodotti in ogni bunch.
bunchoutpudata = []
bunch_print_step = 25
for i,eli in enumerate(crystalconfig):
	for j,elj in enumerate(particles):
		print(f"Crystal in {eli} alignment, {elj} beam")
		
		# Faccio lo stacking di tutti gli eventi presenti nella condizione (i+j)
		# Colonne: nvars (energie layer) + extravars (layer e totale) + 1 (classe)
		tempdatamat = np.zeros((0,nvars+extravars+1))
		numbunches = len(outputdata[i*len(crystalconfig)+j])

		for bunch_ind in range(numbunches):
			if (bunch_ind % bunch_print_step) == 0:
				print(f"Bunch n. {bunch_ind+1} / {numbunches}")

			tempdatamat = np.vstack((tempdatamat,outputdata[i*len(crystalconfig)+j][bunch_ind]))

		bunchoutpudata.append(tempdatamat)
		print(f"")

print(f"Data merging done!\n")

# ================================================================================
#              PLOT DELLO SPETTRO ENERGETICO DEI DATI RIPULITI
# ================================================================================
# Plot della statistica disponibile nei dati estratti
ifplot = True
if ifplot:
	# Faccio vedere la distribuzione degli eventi selezionati in ogni condizione
	fig = plt.figure(figsize = dimfig)
	ax1 = fig.add_subplot(1,2,1)
	ax2 = fig.add_subplot(1,2,2)
	axes = [ax1,ax2]
	figindex = 1
	
	for i,eli in enumerate(crystalconfig):
		for j,elj in enumerate(particles):
			# Creo l'istogramma di frequenze delle energie totali dei dati selezionati
			histo, _ = truehisto1D(bunchoutpudata[2*i+j][:,-1],bins = Ebins)
			histoplotter1D(axes[j],tEbins,histo,r"E$_{tot}$ [GeV]","Counts",f"{eli}, {elj}","best",
																		textfont,crystalcolors[i])

		# Aggiorno l'indice del plot
		figindex += 1

	# Rifinisco la grafica del plot
	fig.tight_layout(rect=[0, 0.03, 1, 0.925])
	fig.suptitle(f'Generated events (after selection)', fontsize = textfont*1.5)
	filename = f"dataprep_redcalo_cleanspectrum"+filetype
	fig.savefig(os.path.join(imgloc,f"{beamtype}",filename),dpi=filedpi)
	plt.close(fig)

# ================================================================================
#						 MERGE AND SHUFFLE DEI DATI ESTRATTI
# ================================================================================
# Merge dei dataset
data_ax = np.vstack((bunchoutpudata[0],bunchoutpudata[1]))
data_am = np.vstack((bunchoutpudata[2],bunchoutpudata[3]))
expected_nrows = len(crystalconfig)*NumBins*NumEventsPerBin
expected_ncols = nvars+extravars+1
print(f"Shape expected dei dataset: ({expected_nrows}, {expected_ncols})")
print(f"Shape effettiva dei dataset:")
print(f"\t data_ax: \t{np.shape(data_ax)}")
print(f"\t data_am: \t{np.shape(data_am)}")

# Shuffle
data_ax_shuffle = sk_utils.shuffle(data_ax)
data_am_shuffle = sk_utils.shuffle(data_am)

# Plot di controllo pre-shuffle
ifplot = True
if ifplot:
	fig = plt.figure(figsize = dimfig)
	ax = fig.add_subplot(1,2,1)
	ax.plot(data_ax[:,0], color = 'mediumblue', label = 'Axial',linestyle = '', marker = '.', 
			markersize = 2.5)
	ax.plot(data_am[:,0], color = 'red', label = 'Random',linestyle = '', marker = '.', 
			markersize = 2.5)
	ax.set_xlabel('Event number',fontsize = textfont)
	ax.set_ylabel(r'Class (0 = n, 1 = $\gamma$)',fontsize = textfont)
	leg = ax.legend(loc = 'center left', fontsize = 1.25*textfont)
	leg.legend_handles[0]._sizes = [100]
	leg.legend_handles[1]._sizes = [100]
	ax.tick_params(labelsize = textfont*0.8)
	ax.set_title('Pre-shuffle',fontsize=textfont*1.5)
	
	# Post-shuffle
	ax = fig.add_subplot(1,2,2)
	ax.plot(data_ax_shuffle[:,0], color = 'mediumblue', label = 'Axial',linestyle = '', marker = '.', 
			markersize = 2.5)
	ax.plot(data_am_shuffle[:,0], color = 'red', label = 'Random',linestyle = '', marker = '.', 
			markersize = 2.5)
	ax.set_xlabel('Event number',fontsize = textfont)
	ax.set_ylabel(r'Class value ($0 = \gamma, 1 = n$)',fontsize = textfont)
	leg = ax.legend(loc = 'center left', fontsize = 1.25*textfont)
	leg.legend_handles[0]._sizes = [100]
	leg.legend_handles[1]._sizes = [100]
	ax.tick_params(labelsize = textfont*0.8)
	ax.set_title('Post-shuffle',fontsize=textfont*1.5)
	
	# Rifinisco la grafica
	fig.set_tight_layout('tight')
	filename = f"dataprep_redcalo_shuffledata"+filetype
	fig.savefig(os.path.join(imgloc,f"{beamtype}",filename),dpi=filedpi)
	plt.close(fig)
	#plt.show()
	
# ================================================================================
#							SALVATAGGIO DEI DATI ESTRATTI
# ================================================================================
# Definisco il file di output indicando la risoluzione energetica
outfilename = fileprename + 'eqmerged_data_redcalo_' + beamtype + '.npz'
print(f"Saving the output file...")

np.savez_compressed(outfilename,
					data_ax = data_ax_shuffle,
					data_am = data_am_shuffle,
					Ebins = Ebins,
					tEbins = tEbins,
					NumBins = NumBins,
					NumEventsPerBin = NumEventsPerBin,
					ifnorma = ifnorma)
print(f"File saved at:")
print(fileprename)
print(f"")
print(f"With name:")
print(outfilename[len(fileprename):])
print(f"Job done!\n")