# ==========================================================================================
#                                  PREMESSA (sintassi)
# ==========================================================================================
# Questo script acquisisce in input quattro file dati raw generati via Geant4, contenenti
# i risultati delle simulazioni eseguite con due tipo di particelle diverse (fotoni, neutroni)
# e in due condizioni diverse (random, asse). Lo script estrae gli eventi, calcola per ciascuno
# il deposito energetico in ogni layer e in totale e mostra la distribuzione di energia totale
# depositata nel sistema.
#
# Sintassi di call:
#   python dataplotter.py [beamtype] [ifdo]
#
# Esempio di call:
# $ conda activate pypmg
# $ cd "C:\Users\Pietro\Desktop\Academic Research\Analisi dati\Analysis_projects\2023\Simulazioni Cristalli Orientati\proto-oriented-sac_analysis"
# $ python dataplotter.py unif_g201_unif_h201 True
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt        # Per ambienti di plot
import matplotlib.colors as colors     # Per i colori dei plot
import scipy.io as io                  # Per particolari funzioni di I/O
import scipy.signal as sig             # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg           # Per analisi dei segnali e delle immagini
import os                              # Per agire sul sistema operativo
import sys                             # [come sopra]
import numpy as np                     # Per analisi numeriche
import pandas as pd                    # Per analisi numeriche con i dataframe
import time                            # Per il timing degli script
import gc                              # Per la pulizia della memoria
import uproot                          # Per l'upload dei file root
import lmfit                           # Per fit avazati
import json                            # Per leggere i file JSON
import PyPDF2                          # Per unire i PDF alla fine
import glob                            # Per manipolazione dei path
import h5py                            # Per caricare i file .h5
import copy                            # Per copiare gli oggetti in modo corretto
import subprocess                      # Per chiamare script come da bash
import matplotlib as mpl               # Per rendering dei plot
from sklearn import utils as sk_utils  # Per AI

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
sys.path.append("/eos/user/p/pmontigu/Functions/")
sys.path.append("C:\\Users\\Pietro\\Desktop\\Academic Research\\Analisi dati\\Functions\\")
sys.path.append("D:\\Academic Research\\Analisi dati\\Functions\\")
from MyFunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
mpl.rcParams['figure.dpi'] = 300             # Per evitare che i plot inline siamo smearati

# =========================================================================================
#                                      DEFINIZIONI                                      
# =========================================================================================
# Definisco le dimensioni dei font e delle figure e altre variabili generali
textfont = 20         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.png'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filedpi = 540         # Risoluzione in caso di salvataggio di png/jpeg
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                                  DATI DALLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = 'statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Estraggo tutte le variabili di interesse dal file di status
ifVM = statusdata['ifVM']
fileprename = statusdata['fileprename']
n_jobs = statusdata['n_jobs']
imgloc = statusdata['imgloc']
temploc = statusdata['temploc']

# ================================================================================
#              INFORMAZIONI PRELIMINARI SULL'ESTRAZIONE DATI
# ================================================================================
# Definisco il fascio da usare
beamtype = sys.argv[1]

# Definisco le configurazioni esistenti
crystalconfig = ['axial','amorphous']
crystalconfignames = ['Axial','Random']
particles = ['gamma','neutron']
crystalcolors = ['mediumblue','red']

# Quante variabili raw esistono nei file dati?
nvars = 100         # 4 layer da 5x5 cristalli

# Quante variabili aggiungere oltre a quelle raw?
extravars = 5       # 4 energie per layer, 1 energia totale

# Nel nome delle key dei dati salvati, qual è l'indice (della stringa)
# che indica il layer di appartenenza?
layerid = 10

# Faccio tutta l'estrazione o riguardo dati già estratti?
try:
    ifdo = (sys.argv[2] == 'True')
except:
    ifdo = True

# Definisco il path dove si trovano o troveranno i dati già estratti
outfilename = os.path.join(temploc,'dataplotter.npz')

# ================================================================================
#              INFORMAZIONI PRELIMINARI SULL'APERTURA FILE
# ================================================================================
# Apro tutti i file in una volta o in bunch di eventi?
# N.B. il numero di eventi per bunch deve essere un sottomultiplo del numero di eventi
# in ogni run... (e.g., se faccio run da 250k eventi, ok bunch da 50k o 125k, no ok 100k)
NumEventsPerBunch = 10000 # Quanti eventi per bunch
NumBunchesForPrint = 25

# ================================================================================
#              INFORMAZIONI PRELIMINARI SULLA SELEZIONE EVENTI
# ================================================================================
# Vettore di binning per gli spettri energetici
Emin = 1        # GeV
Emax = 151      # GeV
Ebinstep = 2    # GeV
Ebins = np.arange(Emin,Emax+Ebinstep,Ebinstep)
tEbins = truebins(Ebins)
NumBins = len(tEbins)

# ================================================================================
#              PLOT DELLO SPETTRO ENERGETICO DEI DATI RAW
# ================================================================================
# Plot della statistica disponibile nei file raw
print(f"Plotting the energy spectrum of the raw data...\n")
if ifdo:
	# Procedo con l'estrazione dei dati grezzi.
	outputdata = []

	for i,eli in enumerate(crystalconfig):
		for j,elj in enumerate(particles):
			# Alloco lo slot per i dati prodotti in questa configurazione
			outputdata.append(np.zeros_like(tEbins))
			
			# ================================================================================
			#                            ESTRAZIONE DATI
			# ================================================================================
			# Definisco il nome del file da caricare
			filepostname = 'tbeamdata_' + eli + '_' + elj + '_' + beamtype + '.root'
			print(f"Crystal in {eli} alignment, {elj} beam")
			print(f"Filename: {filepostname}")
			
			# Apro il file e calcolo il numero di eventi a disposizione
			inputfile = uproot.open(fileprename + filepostname)['outData']
			nevents = inputfile['NEvent'].num_entries
			print(f"Eventi disponibili: {nevents}")
			
			# ================================================================================
			#                            BUNCHING
			# ================================================================================
			# Itero su ogni bunch di dati
			numbunches = int(np.ceil(nevents/NumEventsPerBunch))

			for bunch_ind in range(numbunches):
				if (bunch_ind % NumBunchesForPrint == 0):
					print(f"Bunch n. {bunch_ind+1} / {numbunches}")

				# Preinizializzo la matrice da riempire con i dati del file
				# N.B. in realtà è un vettore: un'energia totale per ciascun evento
				# (non salvo le energie parziali)
				partial_energies = np.zeros((NumEventsPerBunch,),dtype=np.float32)

				# Estraggo le energie depositate in ogni cristallo evento per evento.
				for k,elk in enumerate(inputfile.keys()):
					# Estraggo e salvo l'energia depositata in totale.
					if (elk != 'NEvent') & (elk != 'Seed0') & (elk != 'Seed1') & (elk != 'TotalEnergyOfPrimaryParticle'):
						# Estraggo l'energia del layer.
						energy_crys = np.array(inputfile[elk].array(entry_start = NumEventsPerBunch*bunch_ind,
											 						entry_stop = NumEventsPerBunch*(bunch_ind+1)),
											 						dtype = np.float32)

						# Aggiorno le energie parziali di ogni layer
						partial_energies += energy_crys         # Aumento l'energia totale

				# Calcolo e aggiorno il numero di eventi per bin energetico
				histo, _ = truehisto1D(partial_energies,bins = Ebins)
				outputdata[-1] += histo
					
			# Chiudo il file a fine iterazione
			inputfile.close()
			print(f"Done!\n")
else:
	# Carico dati vecchi da plottare
	print(f"Loading the processed histograms...")
	outputdata = np.load(outfilename)['outputdata']

# ================================================================================
#                            ISTOGRAMMI
# ================================================================================
# A fine ciclo, mostro gli istogrammi di energia depositata
NumEventsThr = 10000

fig = plt.figure(figsize = dimfig)
for i,eli in enumerate(crystalconfig):
	ax = fig.add_subplot(1,2,i+1)
	for j,elj in enumerate(particles):
		histoplotter1D(ax,tEbins,outputdata[2*i+j],r"E$_{tot}$ [GeV]","Counts",
					   f"Data ({crystalconfignames[i]}, {elj})","best",textfont,crystalcolors[j])
		if j == 1:
			ax.plot(tEbins,tEbins*0+NumEventsThr,color='Black',linestyle='--',linewidth=2,
			label = f"{NumEventsThr} events/bin")
		else:
			ax.plot(tEbins,tEbins*0+NumEventsThr,color='Black',linestyle='--',linewidth=2)
		ax.set_yscale('log')
		#ax.set_ylim([9*10**3,2*10**6])
		ax.legend(loc='upper right',fontsize=textfont*0.75,framealpha=1)

		# Printout per referenza
		print(f"{eli}, {elj}: {outputdata[2*i+j][-1]} eventi nell'ultimo bin\n")

# Rifinisco la grafica del plot
fig.tight_layout(rect=[0, 0.03, 1, 0.925])
fig.suptitle(f'Generated events (before selection)', fontsize = textfont*1.5)
filename = f"dataprep_rawspectrum_Emin_{Emin}_Emax_{Emax}_Ebinstep_{Ebinstep}" + filetype
fig.savefig(os.path.join(imgloc,f"{beamtype}",filename),dpi=filedpi)
plt.close(fig)
#plt.show()

# ================================================================================
#                            SALVATAGGIO
# ================================================================================
# Salvo gli output temporanei
np.savez_compressed(outfilename, outputdata = np.array(outputdata))
print(f"Output file saved at:")
print(outfilename)

# Printout di controllo
print(f"Plotting done!\n")
print(f"Job done!\n")