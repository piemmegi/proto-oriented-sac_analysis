# ==========================================================================================
#                                  PREMESSA (sintassi)
# ==========================================================================================
# Questo script analizza l'output di "classifier_test" e produce il report finale sulle
# capacità di classificazione di un classificatore ottimizzato, in funzione dell'energia
# totale depositata nel calorimetro. In questo caso, si producono:
#       - Curve di Accuracy vs Energia totale (normali e diff)
#       - Curve ROC (normali e diff)
#
# Sintassi di call:
#   python classifier_finalreport.py [classifiertype] [beamtype]
#
# Esempio di call:
# $ conda activate pypmg
# $ cd "C:\Users\Pietro\Desktop\Academic Research\Analisi dati\Analysis_projects\2023\Simulazioni Cristalli Orientati\proto-oriented-sac_analysis"
# $ python classifier_finalreport.py rf unif_g251_unif_h501
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt      # Per ambienti di plot
import matplotlib.colors as colors   # Per i colori dei plot
import scipy.io as io                # Per particolari funzioni di I/O
import scipy.signal as sig           # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg         # Per analisi dei segnali e delle immagini
import os                            # Per agire sul sistema operativo
import sys                           # [come sopra]
import numpy as np                   # Per analisi numeriche
import pandas as pd                  # Per analisi numeriche con i dataframe
import time                          # Per il timing degli script
import gc                            # Per la pulizia della memoria
import uproot                        # Per l'upload dei file root
import lmfit                         # Per fit avazati
import json                          # Per leggere i file JSON
import PyPDF2                        # Per unire i PDF alla fine
import glob                          # Per manipolazione dei path
import h5py                          # Per caricare i file .h5
import copy                          # Per copiare gli oggetti in modo corretto
import subprocess                    # Per chiamare script come da bash
import matplotlib as mpl             # Per rendering dei plot
import inspect                       # Per ispezione dei codici
import warnings                      # Per disattivare i Deprecation Warning
import seaborn                       # Per plot aesthetic
from sklearn import preprocessing    # ML / preprocessing
from sklearn import model_selection  # ML / divisione in TrS e TeS
from sklearn import decomposition    # ML / PCA
from sklearn import neighbors        # ML / KNN
from sklearn import metrics          # ML / Metriche di scoring
from sklearn import ensemble         # ML / Random forests

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
sys.path.append("/eos/user/p/pmontigu/Functions/")
sys.path.append("C:\\Users\\Pietro\\Desktop\\Academic Research\\Analisi dati\\Functions\\")
sys.path.append("D:\\Academic Research\\Analisi dati\\Functions\\")
from MyFunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
mpl.rcParams['figure.dpi'] = 300             # Per evitare che i plot inline siamo smearati
warnings.filterwarnings("ignore", category=DeprecationWarning)

# =========================================================================================
#                                      DEFINIZIONI                                      
# =========================================================================================
# Definisco le dimensioni dei font e delle figure e altre variabili generali
textfont = 25         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filedpi = 540         # Risoluzione in caso di salvataggio di png/jpeg
nptsfit = 100000      # Numero di punti per il plot dei fit

# =========================================================================================
#                                      CUSTOM FUNCTIONS                                      
# =========================================================================================
def custom_histoplotter2D(ax,binsX,binsY,histo2D,colmap = 'jet',slope = None,xlabel = 'X axis',ylabel = 'Y axis',
                                  textfont = 15,iflog = False,vmin = None,vmax = None,ifcbarfont = False,cbarlabel='Z axis'):
    # Deduce what is the figure from which "ax" is drawn
    f = ax.get_figure()
    
    # Define the colormap and set the bad pixels to the same color of the minimum of the scale.
    my_cmap = eval('copy.copy(pltmaps.' + colmap + ')')
    my_cmap.set_bad(my_cmap(0))

    # Plot the 2D histogram transposed. Note that this is due to how plt.imshow() works, and it assumes
    # that the histogram has been produced with np.histogram2d() or equally with truehisto2D().
    # Choose the normalization as defined by the user.
    if iflog:
        # Assume a log scale and normalize as a consequence.
        thisnorm = colors.LogNorm(vmin = vmin, vmax = vmax)
    else:
        # Assume a linear scale, which can be either of the TwoSlope type or not
        if slope is None:
            thisnorm = colors.Normalize(vmin = vmin, vmax = vmax)
        else:
            thisnorm = colors.TwoSlopeNorm(slope, vmin = vmin, vmax = vmax)

    imhisto = ax.imshow(histo2D.transpose(),cmap = my_cmap,norm=thisnorm,aspect = 'auto',
                                        extent = [binsX[0],binsX[-1],binsY[-1],binsY[0]])

    # Set labels, grid, colorbar and axis propeties.
    ax.set_xlabel(xlabel,fontsize = textfont)
    ax.set_ylabel(ylabel,fontsize = textfont)
    ax.grid(linewidth=0.5)
    cb = f.colorbar(imhisto,ax=ax)
    cb.set_label(cbarlabel,fontsize=textfont*1.25,rotation=270,labelpad=35)
    if ifcbarfont:
        # Set the fontsize for the colorbar
        for el in cb.ax.get_yticklabels():
            el.set_fontsize(0.75*textfont)
    ax.tick_params(labelsize = 0.75*textfont)

    # If there exists only one bin in either direction, set a particular limit for the axis. 
    # Otherwise, use the bins edges.
    if np.min(binsX) == np.max(binsX):
        ax.set_xlim([np.min(binsX)-5,np.max(binsX)+5])
    else:
        ax.set_xlim([np.min(binsX),np.max(binsX)])
        
    if np.min(binsY) == np.max(binsY):
        ax.set_ylim([np.min(binsY)-5,np.max(binsY)+5])
    else:
        ax.set_ylim([np.min(binsY),np.max(binsY)])
    return()

# ==========================================================================================
#                                  DATI DALLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = 'statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
ifVM = statusdata['ifVM']
fileprename = statusdata['fileprename']
imgloc = statusdata['imgloc']
temploc = statusdata['temploc']

# ==========================================================================================
#                                  INPUT DELL'ALGORITMO
# ==========================================================================================
# In che condizioni guardo il cristallo?
crystalconds = ['am','ax']

# Che classificatore uso?
classifiertype = sys.argv[1]

# Tipo di fascio da analizzare
beamtype = sys.argv[2]

# In base al fascio scelto, cambio label per le energie ("tEbins" può contenere infatti
# o energie depoistate o energie iniziali)
if (beamtype == 'unif1_251unif1_251large') | (beamtype == 'unif_g251_unif_h501'):
    Estring = r"Deposited energy (E$_{dep}$) [GeV]"
    ifEin = False
else:
    Estring = r"Incident particle energy (E$_{in}$) [GeV]"
    ifEin = True

# Definisco altre variabili utili per l'analisi
crystalcondnames = ['Random alignment','Axial alignment']


# ==========================================================================================
#                                      DEFINIZIONE COLORI
# ==========================================================================================
# Costruisco colori per gli scatter plot che siano BW compliant
gnuplot_cmap = plt.get_cmap('gnuplot2')
indices = np.linspace(0, gnuplot_cmap.N, 20)
my_colors = [gnuplot_cmap(int(i)) for i in indices]
crystalcondcolors = [my_colors[4],my_colors[13]] # Blu, Rosso

# Definisco la mappa di colore per gli histo2D che sia color compliant
colmap = 'gnuplot2'

# ==========================================================================================
#                                      ESTRAZIONE DATI 
# ==========================================================================================
# Definisco se sottocampionare le curve ROC
ROCsubstep = 1

# Itero su ogni condizione di allineamento del cristallo e ne estraggo gli score rilevanti
all_accuracy = []           # Accuracy
all_std_accuracy = []

all_roc_grid = []           # x e y delle curve ROC
all_roc_curves = []
all_roc_errs = []
all_auc = []                # AUC
all_auc_errs = []

tEbins = []                 # Binning energetici
forcedEmin = 26             # Cut inferiore in energia

for crystalcond in crystalconds:
    # Apro il file npz contenente l'output della K-Fold validation per estrarre i risultati 
    # di accuratezza averaged
    filename = os.path.join(temploc,f'opt__results__{crystalcond}__{classifiertype}__{beamtype}.npz')
    print(f"Data loading: {crystalcond} mode")

    with np.load(filename,allow_pickle=True) as gsdata:
        # Estraggo i binning energetici
        theBins = gsdata['tEbins']
        cutcond = (theBins >= forcedEmin)
        tEbins.append(gsdata['tEbins'][cutcond])

        # Estraggo gli score
        all_accuracy.append(gsdata['av_classifier_accuracy'][cutcond])
        all_std_accuracy.append(gsdata['std_classifier_accuracy'][cutcond])
        
        # Estraggo le curve ROC
        all_roc_grid.append(gsdata['roc_grid'][::ROCsubstep])
        all_roc_curves.append(gsdata['av_roc_global'][::ROCsubstep,cutcond])
        all_roc_errs.append(gsdata['std_roc_global'][::ROCsubstep,cutcond])

        # Estraggo le AUC
        all_auc.append(gsdata['av_auc_global'][cutcond])
        all_auc_errs.append(gsdata['std_auc_global'][cutcond])
    print(f"Extraction done!\n")
    
print(f"Beginning the plotting phase...")

# ==========================================================================================
#                                 PLOT CURVE DI ACCURATEZZA 
# ==========================================================================================
# Mostro le curve di accuratezza per ogni condizione del cristallo
fig, ax = plt.subplots(figsize = dimfig)

for crystal_ind, crystalcond in enumerate(crystalconds):
    ax.errorbar(tEbins[crystal_ind],100*all_accuracy[crystal_ind],100*all_std_accuracy[crystal_ind],
            marker='.',markersize=10,capsize=5,linestyle='-',linewidth=1.25,
            label=crystalcondnames[crystal_ind],color=crystalcondcolors[crystal_ind])

ax.set_xlabel(Estring,fontsize = textfont)
ax.set_ylabel('K-averaged accuracy [%]',fontsize = textfont)
ax.grid(linewidth=0.5)
ax.tick_params(labelsize = 0.75*textfont)
if ifEin:
    ax.legend(fontsize=textfont, loc = 'lower center',framealpha=1)
else:
    ax.legend(fontsize=textfont, loc = 'lower left',framealpha=1)
fig.set_tight_layout('tight')
figfilename = f'{classifiertype}_results_allacc'+filetype
fig.savefig(os.path.join(imgloc,f"{beamtype}",figfilename),dpi=filedpi)
plt.close(fig)
print(f"Plot of the accuracy curve: done!")

# ==========================================================================================
#                          PLOT CURVE DI INCREMENTO DI ACCURATEZZA 
# ==========================================================================================
# Calcolo gli incrementi percentuali di accuratezza
acc_increases = (all_accuracy[1]-all_accuracy[0])*100
err_acc_increases = np.sqrt(all_std_accuracy[1]**2 + all_std_accuracy[0]**2)*100

# Fitto gli andamenti con una funzione esponenziale
try:
    start_pts = [-10,0.01,10]   # Starting point per i fit esponenzialiresult, xt
    result, xth, yth = fitter_exp(tEbins[0],acc_increases,err_acc_increases,
                                  iftail = True,start_pts = start_pts,ifset_lowlims = False)
    #fitlabel = r'Fitted curve: $a \cdot e^{-b \cdot x} + c$'
    fitlabel = r'Exponential fit ($y = a \cdot e^{-b \cdot x} + c$)'
except:
    # Se il fit è fallito, produco una curva filler
    xth = np.linspace(np.min(tEbins[0]),np.max(tEbins[0]),nptsfit)
    yth = xth_fnr*0
    label = r'Fit failed'

# Mostro le curve di incremento di accuratezza in una figura diversa
fig, ax = plt.subplots(figsize = dimfig)
ax.errorbar(tEbins[0],acc_increases,err_acc_increases,
             marker='.',markersize=5,capsize=5,linestyle='-',linewidth=1,
             color='mediumblue',label='Data')
ax.plot(xth,yth,color = 'red', linestyle = '--', label = fitlabel,linewidth = 2)
ax.set_xlabel(Estring,fontsize = textfont)
ax.set_ylabel('K-averaged increase of accuracy [%]',fontsize = textfont)
ax.grid(linewidth=0.5)
ax.tick_params(labelsize = 0.75*textfont)
ax.legend(fontsize=textfont, loc = 'lower right',framealpha=1)
fig.set_tight_layout('tight')
figfilename = f'{classifiertype}_results_allaccincreases'+filetype
fig.savefig(os.path.join(imgloc,f"{beamtype}",figfilename),dpi=filedpi)
plt.close(fig)
print(f"Plot of the accuracy increase curve: done!")

# ==========================================================================================
#                          PLOT CURVE ROC
# ==========================================================================================
# Definisco le energie a cui plottare le curve ROC
all_energies = [30,60,90,120]
all_energy_colors = ['mediumblue','cyan','darkorange','red']

# Definisco altre variabili utili
iferr = False         # Mostro le barre d'errore sulle curve?
ind0 = 0             # Calcolo gli incrementi di curva ROC come (ind1)-(ind0): qui è ax-am
ind1 = 1
conf = 1             # Barre d'errore come confidence level

# Mostro le curve ROC per alcuni casi notevoli e il loro incremento in valore assoluto
fig1, ax1 = plt.subplots(figsize = dimfig)
for k,thr_energy in enumerate(all_energies):
    # Calcolo l'indice corrispondente alla threshold di energia
    ene_ind = np.where(tEbins[0] >= thr_energy)[0][0]

    # ======================================================================================
    # Plot della curva ROC
    fig2, ax2 = plt.subplots(figsize = dimfig)

    for crystal_ind,crystalcond in enumerate(crystalconds):
        #crlabel = crystalcondnames[crystal_ind] + \
        #          f", AUC = {all_auc[crystal_ind][ene_ind]:.4f} " + \
        #          r"$\pm$" + f"{all_auc_errs[crystal_ind][ene_ind]:.4f}"
        crlabel = crystalcondnames[crystal_ind] + f" (AUC = {100*all_auc[crystal_ind][ene_ind]:.2f} %)"
        ax2.plot(all_roc_grid[crystal_ind]*100,
                    all_roc_curves[crystal_ind][:,ene_ind]*100,
                    #marker='.',markersize=0.5,linestyle='',
                    marker='',linestyle='-',linewidth=2,
                    color=crystalcondcolors[crystal_ind], 
                    label = crlabel)

        if iferr:
            # Plot delle barre d'errore, se richiesto
            ax2.fill_between(all_roc_grid[crystal_ind]*100,
                            (all_roc_curves[crystal_ind][:,ene_ind] - conf * all_roc_errs[crystal_ind][:,ene_ind]) *100,
                            (all_roc_curves[crystal_ind][:,ene_ind] + conf * all_roc_errs[crystal_ind][:,ene_ind]) *100,
                            color=crystalcondcolors[crystal_ind],
                            alpha=0.2,
                            label=f"{conf}" + r"$\sigma$ C.L.")
    # Rifinisco la grafica
    ax2.set_xlabel('FPR [%]',fontsize = textfont)
    ax2.set_ylabel('TPR [%]',fontsize = textfont)
    ax2.set_xlim([0,100])
    if ifEin:
        ax2.set_xlim([0,10])
        ax2.set_ylim([95,100])
        leg2 = ax2.legend(fontsize=0.8*textfont, loc = 'lower center',framealpha=1)
    else:
        ax2.set_ylim([0,100])
        ax2.plot(all_roc_grid[crystal_ind]*100,
                     all_roc_grid[crystal_ind]*100,
                     color = 'black', linestyle = '--', linewidth = 2,
                     label = 'Chance level (AUC = 50 %)')
        leg2 = ax2.legend(fontsize=0.8*textfont, loc = 'lower right',framealpha=1)
    
    for i in range(len(leg2.legend_handles)):
        leg2.legend_handles[i]._markersize = 15
    ax2.grid(linewidth=0.5)
    ax2.tick_params(labelsize = 0.75*textfont)
    fig2.set_tight_layout('tight')
    figfilename = f'{classifiertype}_results_ROCcurves_{thr_energy:.0f}GeV'+filetype
    fig2.savefig(os.path.join(imgloc,f"{beamtype}",figfilename),dpi=filedpi)
    plt.close(fig2)

    # ==========================================================================================
    # Calcolo l'incremento della curva ROC
    diff_curve = (all_roc_curves[ind1][:,ene_ind]-all_roc_curves[ind0][:,ene_ind])*100
    diff_err = np.sqrt( (all_roc_errs[ind1][:,ene_ind])**2 + (all_roc_errs[ind0][:,ene_ind])**2 )*100
    diff_auc = all_auc[ind1][ene_ind] - all_auc[ind0][ene_ind]
    diff_auc_err = np.sqrt( (all_auc_errs[ind1][ene_ind])**2 + (all_auc_errs[ind0][ene_ind])**2 )
    crlabel = f"{thr_energy} GeV" #+ r"$\Delta$" + f" AUC = {diff_auc:.4f} +/- {diff_auc_err:.4f}"

    # Mostro l'incremento della curva ROC
    ax1.plot(all_roc_grid[0]*100,diff_curve,
                 #marker='.',markersize=0.5,linestyle='',
                 marker='',linestyle='-',linewidth=2,
                 color = all_energy_colors[k], label = crlabel)

    if iferr:
        # Plot delle barre d'errore, se richiesto
        ax1.fill_between(all_roc_grid[0]*100,
                        (diff_curve - conf * diff_err),
                        (diff_curve + conf * diff_err),
                        color=all_energy_colors[k],
                        alpha=0.2,
                        label=f"{conf}" + r"$\sigma$ C.L.")

# Rifinisco la grafica della curva di incremento
ax1.set_xlabel('FPR [%]',fontsize = textfont)
ax1.set_ylabel('TPR increase [%]',fontsize = textfont)

if ifEin:
    ax1.set_xlim([0,10])
    ax1.set_ylim([0,20])
else:
    ax1.set_xlim([0,100])
    ax1.set_ylim([0,35])
ax1.grid(linewidth=0.5)
ax1.tick_params(labelsize = 0.75*textfont)
leg1 = ax1.legend(fontsize=textfont, loc = 'upper right',framealpha=1)
for i in range(len(leg1.legend_handles)):
    leg1.legend_handles[i]._markersize = 15
fig1.set_tight_layout('tight')
figfilename = f'{classifiertype}_results_ROCcurves_increases1D'+filetype
fig1.savefig(os.path.join(imgloc,f"{beamtype}",figfilename),dpi=filedpi)
plt.close(fig1) 

print(f"Plot of the ROC and ROC increase curves: done!")

# ==========================================================================================
#                          PLOT INCREMENTO CURVE ROC 2D
# ==========================================================================================
# Calcolo la distribuzione 2D di incremento della curva ROC
ind0 = 0             # Calcolo gli incrementi di curva ROC come (ind1)-(ind0): qui è ax-am
ind1 = 1
diff_curve2D = (all_roc_curves[ind1]-all_roc_curves[ind0])*100 # Righe = fpr, Colonne = energia
diff_curve2D[diff_curve2D < 0] = 0

# Definisco alcuni parametri per il plot
slope = np.max(diff_curve2D)/2
iflog = False

# Mostro il plot 2D
fig, ax = plt.subplots(figsize = dimfig)
custom_histoplotter2D(ax,all_roc_grid[0]*100,tEbins[0],diff_curve2D,colmap,slope,'FPR [%]',
                      Estring,textfont,iflog,ifcbarfont=True,cbarlabel='TPR increase [%]')
#ax.set_title('TPR increase [%]',fontsize = 2*textfont)
if ifEin:
    ax.set_xlim([0,5])
else:
    ax.set_xlim([0,40])
fig.set_tight_layout('tight')
figfilename = f'{classifiertype}_results_ROCcurves_increases2D'+filetype
fig.savefig(os.path.join(imgloc,f"{beamtype}",figfilename),dpi=filedpi)
plt.close(fig)

print(f"Plot of the 2D map of the ROC increase curve: done!")

# ==========================================================================================
#                          CONCLUDE
# ==========================================================================================
print(f"")
print(f"Job done!\n")