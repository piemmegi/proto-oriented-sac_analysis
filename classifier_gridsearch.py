# ==========================================================================================
#                                  PREMESSA (sintassi)
# ==========================================================================================
# Questo script analizza i dataset prodotti da "datapreparator.py" ed esegue particle identification
# usando un algorithmo di Machine Learning arbitrario. Questo script serve specificamente a
# ottimizzare gli algoritmi, eseguendo una Grid Search sui loro ipeprarametri. A ogni punto
# della Grid Search si esegue una 5-Fold Cross-Validation e si determina la accuracy media
# di classificazione, dopodiché si salva il risultato parziale e si prosegue.
#
# Sintassi di call:
#   python classifier_gridsearch.py [classifier] [orientation] [beamtype]
#
# Esempio di call:
# $ conda activate pypmg
# $ cd "C:\Users\Pietro\Desktop\Academic Research\Analisi dati\Analysis_projects\2023\Simulazioni Cristalli Orientati\proto-oriented-sac_analysis"
# $ python classifier_gridsearch.py rf am unif_g251_unif_h501
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import di moduli interni
import matplotlib.pyplot as plt      # Per ambienti di plot
import matplotlib.colors as colors   # Per i colori dei plot
import scipy.io as io                # Per particolari funzioni di I/O
import scipy.signal as sig           # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg         # Per analisi dei segnali e delle immagini
import os                            # Per agire sul sistema operativo
import sys                           # [come sopra]
import numpy as np                   # Per analisi numeriche
import pandas as pd                  # Per analisi numeriche con i dataframe
import time                          # Per il timing degli script
import gc                            # Per la pulizia della memoria
import uproot                        # Per l'upload dei file root
import lmfit                         # Per fit avazati
import json                          # Per leggere i file JSON
import PyPDF2                        # Per unire i PDF alla fine
import glob                          # Per manipolazione dei path
import h5py                          # Per caricare i file .h5
import copy                          # Per copiare gli oggetti in modo corretto
import subprocess                    # Per chiamare script come da bash
import matplotlib as mpl             # Per rendering dei plot
import inspect                       # Per ispezione dei codici
import warnings                      # Per disattivare i Deprecation Warning
import seaborn                       # Per plot aesthetic
from sklearn import preprocessing    # ML / preprocessing
from sklearn import model_selection  # ML / divisione in TrS e TeS
from sklearn import decomposition    # ML / PCA
from sklearn import neighbors        # ML / KNN
from sklearn import metrics          # ML / Metriche di scoring
from sklearn import ensemble         # ML / Random forests

# Import di funzioni singole o funzioni personali, dal file "MyFunctions.py"
sys.path.append("/eos/user/p/pmontigu/Functions/")
sys.path.append("C:\\Users\\Pietro\\Desktop\\Academic Research\\Analisi dati\\Functions\\")
sys.path.append("D:\\Academic Research\\Analisi dati\\Functions\\")
from MyFunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
mpl.rcParams['figure.dpi'] = 300             # Per evitare che i plot inline siamo smearati
warnings.filterwarnings("ignore", category=DeprecationWarning)

# =========================================================================================
#                                      DEFINIZIONI                                      
# =========================================================================================
# Definisco le dimensioni dei font e delle figure e altre variabili generali
textfont = 20         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.png'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filedpi = 540         # Risoluzione in caso di salvataggio di png/jpeg
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                                  DATI DALLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = 'statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
ifVM = statusdata['ifVM']
fileprename = statusdata['fileprename']
n_jobs = statusdata['n_jobs']
random_state = statusdata['random_state']
imgloc = statusdata['imgloc']
temploc = statusdata['temploc']

# ==============================================================================
#                                LOAD DEI DATI
# ==============================================================================
# Definisco il classificatore
classifiertype = sys.argv[1]

# Definisco se sono in amorfo o in assiale
crystalcond = sys.argv[2]

# Definisco il tipo di fascio
beamtype = sys.argv[3]

# Apro il file dati e lo spacchetto in dataset + codifica
t0 = time.perf_counter()
print(f"Loading file...")

filename = fileprename + 'eqmerged_data_' + beamtype + '.npz'
with np.load(filename) as dataset:
    # Creo un dataframe a partire dal dataset
    df = pd.DataFrame(dataset['data_' + crystalcond],dtype=np.float32)

    print(f"Elapsed time (for data loading and dataframe conversion): {(time.perf_counter()-t0):.2f} s \n")

    # Converto il dataframe in un 2D-array, per renderlo fruibile per analisi
    # future (il dataset originale era un 1D array di tuple...)
    X = np.array(df.iloc[:,1:])    # Ogni riga è un pattern, ogni colonna una feature
    nrows,ncols = np.shape(X)      # Dimensioni del dataset

    # Estraggo il vettore delle classi e lo converto da categorical a numerical
    y = np.int16(df.iloc[:,0])  

    # Mi segno come avviene la conversione
    lut = '1 = gamma, 0 = neutron\n'
    print(f"LUT: {lut}")
    numclasses = 2

# ==============================================================================
#                                GRID SEARCH SETUP
# ==============================================================================
# Definisco gli intervalli per la grid search per ciascun classificatore implementato
# Iniziamo con la Random Forest
RF_n_estimators = [50,100,150,200]          # Random Forest: numero di alberi
RF_criterion = ['gini']#,'entropy']         # Random Forest: criterio di split
RF_max_depth = [None,5,15]                  # Random Forest: massima profondità dei KDT
RF_max_features = ['sqrt',5,15]             # Random Forest: numero di features per split

RF_param_grid = {'n_estimators': RF_n_estimators,       
                 'criterion': RF_criterion,
                 'max_depth': RF_max_depth,
                 'max_features': RF_max_features}

# ==============================================================================
# Neural Network. Nota bene: le architetture sono "congelate", nel senso che il primo
# hidden layer ha sempre 64 neuroni e i seguenti ne hanno sempre 32
NN_hidden_layer_sizes = [np.array([64]),    # NN MLP: neuroni per hidden layer
                         np.array([64,*[32 for _ in range(2)]]),
                         np.array([64,*[32 for _ in range(4)]]),
                         np.array([64,*[32 for _ in range(6)]]),
                         np.array([64,*[32 for _ in range(8)]])]
NN_activation = ['relu','logistic','tanh']  # NN MLP: funzione di attivazione nei layer interni
NN_solver = ['adam']                        # NN MLP: solver per ottimizzare i pesi della rete
NN_alpha = [0.0001,0.001,0.01,0.1]          # NN MLP: termine di regolarizzazione L2
NN_learning_rate_init = [0.001,0.01,0.1]    # NN MLP: learning rate iniziale
NN_max_iter = [1000]

NN_param_grid = {'hidden_layer_sizes': NN_hidden_layer_sizes,
                 'activation': NN_activation,
                 'solver': NN_solver,
                 'alpha': NN_alpha,
                 'learning_rate_init': NN_learning_rate_init,
                 'max_iter': NN_max_iter}

# ==============================================================================
# Definisco che classificatore uso
if (classifiertype == 'rf'):
    param_grid = RF_param_grid
elif (classifiertype == 'nn'):
    param_grid = NN_param_grid

# Definisco altri iperparametri globali per il classificatore e la grid search
n_splits = 5                                   # TrS / TeS splitting (= K nella K-Fold crossvalidation)
kf = model_selection.KFold(n_splits=n_splits)
PCAtype = 'standard'                           # Che PCA usare ("incremental" se Incrementale, standard altrimenti)
thr = 0.95                                     # Cut sulla mCEV per la selezione delle componenti principali
nev = None                                     # Quanti eventi del dataset (= #TrS + #TeS) uso? None = tutti
print(f"Working with: {n_jobs} core(s) \n")

# ==============================================================================
#                                GRID SEARCH
# ==============================================================================
# Se è ben definito, estraggo dal dataset il numero di eventi da analizzare
# (N.B. "nev" = dim(TrS) + dim(TeS))
if nev is not None:
    X = X[:nev,:]
    y = y[:nev]

# Itero su ogni elemento della grid search. Se ho uno step solo, salto
# e lascio a zero tutti gli score.
list_grid = list(model_selection.ParameterGrid(param_grid))
ngridstep = len(list_grid)
t0 = time.perf_counter()

if ngridstep > 1:
    # Ho più di un elemento della GS: procedo regolarmente.
    for j,grid_dict in enumerate(list_grid):
        # ==============================================================================
        #                                PREPARATION
        # ==============================================================================
        # Stampo il passo di iterazione
        print(f"Grid search step n. {j+1} / {len(list_grid)}")
        
        # Controllo se sia necessario saltare l'iterazione. Per convenzione, si salta
        # il passo della GS se esiste già il corrispondente file nella cartella "temploc".
        # Per farlo, definisco quindi il nome del file di output.
        tempoutprename = f"gsout__{beamtype}__{crystalcond}__{classifiertype}"

        if (classifiertype == 'rf'):
            tempoutmidname = f"__{grid_dict['n_estimators']}__{grid_dict['criterion']}__{grid_dict['max_depth']}__{grid_dict['max_features']}__"
        elif (classifiertype == 'nn'):
            numlayers = np.shape(grid_dict['hidden_layer_sizes'])[0]
            tempoutmidname = f"__{numlayers}__{grid_dict['activation']}__{grid_dict['solver']}__{grid_dict['alpha']}__{grid_dict['learning_rate_init']}__{grid_dict['max_iter']}__"

        tempoutpostname = "done.npz"
        tempoutfullname = os.path.join(temploc,tempoutprename+tempoutmidname+tempoutpostname)
        
        # Eseguo il controllo
        if os.path.exists(tempoutfullname):
            print(f"The following output file was found:")
            print(tempoutfullname)
            print(f"Skipping the GS step... \n")
            continue

        # ==============================================================================
        #                                ITERATION
        # ==============================================================================
        # Itero: a ogni passo splitto il dataset in TrS/TeS, preparo i dati, addestro il 
        # classificatore e poi lo uso
        classifier_accuracy = np.zeros((n_splits,))
        
        for i, (train_index, test_index) in enumerate(kf.split(X)):
            # ==============================================================================
            #                                   DATA SPLITTING
            # ==============================================================================
            # Suddivido il dataset in TrS e TeS
            print(f"Step {i+1} / {n_splits} of the K-Fold validation")
            X_train = X[train_index,:]
            y_train = y[train_index]
            X_test = X[test_index,:]
            y_test = y[test_index]

            # ==============================================================================
            #                                   DATA PREPARATION
            # ==============================================================================
            # Definisco il costruttore per lo scaling standard
            scaler = preprocessing.StandardScaler()
            scaler.fit(X_train)
            X_train_scaled = scaler.transform(X_train)

            # Definisco il costruttore per la PCA
            if (PCAtype == 'incremental'):
                pca = decomposition.IncrementalPCA()
            else:
                pca = decomposition.PCA()
            pca.fit(X_train_scaled)
            X_train_pca = pca.transform(X_train_scaled)

            # Calcolo quante componenti principali tenere
            X_ev = pca.explained_variance_ratio_
            X_cumev = np.cumsum(X_ev)
            n_underthr = np.where(X_cumev<=thr)[0].shape[0]
            print(f"Number of Principal Components after the pruning: {n_underthr} / {np.shape(X_cumev)[0]}")

            # Estraggo le features dei dati del TrS dopo scaling e PCA
            X_train_pruned = X_train_pca[:,:n_underthr]
            
            # Estraggo le features dei dati del TeS dopo scaling e PCA    
            X_test_scaled = scaler.transform(X_test)
            X_test_pca = pca.transform(X_test_scaled)
            X_test_pruned = X_test_pca[:,:n_underthr]
            
            # ==============================================================================
            #                                   TRAINING
            # ==============================================================================
            # Creo e addestro il classificatore
            Classifier = ClassifierConstructor(classifiertype,grid_dict,random_state,n_jobs)
            Classifier.fit(X_train_pruned,y_train)

            # ==============================================================================
            #                                CLASSIFICAZIONE
            # ==============================================================================
            # Predico le classi del TeS e le probabilità di classificazione
            y_test_predicted = Classifier.predict(X_test_pruned)
            y_test_predicted_proba = Classifier.predict_proba(X_test_pruned)

            # ==============================================================================
            #                                   SCORE
            # ==============================================================================
            # Scoring
            classifier_accuracy[i] = metrics.accuracy_score(y_test,y_test_predicted)
            
            # Chiudo
            print(f"Done!\n")

        # Salvo gli scoring medi in questa configurazione (= a questo passo di GS) e le loro incertezze
        av_classifier_accuracy = np.mean(classifier_accuracy)
        std_classifier_accuracy = np.std(classifier_accuracy)/np.sqrt(n_splits-1)
        
        # Salvo l'output in un file temporaneo npz
        np.savez_compressed(tempoutfullname, 
                            av_classifier_accuracy = av_classifier_accuracy,      # Accuracy media ad ogni step della GS
                            std_classifier_accuracy = std_classifier_accuracy,    # Errore sulle accuracy
                            random_state = random_state,                          # Inizializzazione del random state
                            n_jobs = n_jobs)                                      # Numero di core usati nel processing
        
        # Printout di controllo
        print(f"Grid Search step output file written at: ")
        print(tempoutfullname)
        print(f"")

        # Stampo il timing corrente
        tend = time.perf_counter()
        print(f"Time elapsed since the beginning of the grid search: {(tend-t0):.3f} s\n")
    
    print(f"Grid search completed!")
else:
    print(f"Only one GS step was defined: no optimization was performed.")

# Salvo tutti gli output di ricerca in npz
print(f"Job done! The output files were written in the folder: ")
print(temploc)
print(f"")