# ==========================================================================================
#                                  PREMESSA (sintassi)
# ==========================================================================================
# Questo script acquisisce in input quattro file dati raw generati via Geant4, contenenti
# i risultati delle simulazioni eseguite con due tipo di particelle diverse (fotoni, neutroni)
# e in due condizioni diverse (random, asse). Lo script estrae gli eventi, calcola per ciascuno
# il deposito energetico in ogni cristallo e mostra la corrispondente distribuzione 3D di
# deposito energetico.
#
# Sintassi di call:
#   python pubplots_shower3D.py
#
# Esempio di call in scala log:
# $ conda activate pypmg
# $ cd "C:\Users\Pietro\Desktop\Academic Research\Analisi dati\Analysis_projects\2023\Simulazioni Cristalli Orientati\proto-oriented-sac_analysis"
#   python pubplots_shower3D.py
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt        # Per ambienti di plot
import matplotlib.colors as colors     # Per i colori dei plot
import scipy.io as io                  # Per particolari funzioni di I/O
import scipy.signal as sig             # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg           # Per analisi dei segnali e delle immagini
import os                              # Per agire sul sistema operativo
import sys                             # [come sopra]
import numpy as np                     # Per analisi numeriche
import pandas as pd                    # Per analisi numeriche con i dataframe
import time                            # Per il timing degli script
import gc                              # Per la pulizia della memoria
import uproot                          # Per l'upload dei file root
import lmfit                           # Per fit avazati
import json                            # Per leggere i file JSON
import PyPDF2                          # Per unire i PDF alla fine
import glob                            # Per manipolazione dei path
import h5py                            # Per caricare i file .h5
import copy                            # Per copiare gli oggetti in modo corretto
import subprocess                      # Per chiamare script come da bash
import matplotlib as mpl               # Per rendering dei plot
from sklearn import utils as sk_utils  # Per AI

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
sys.path.append("/eos/user/p/pmontigu/Functions/")
sys.path.append("C:\\Users\\Pietro\\Desktop\\Academic Research\\Analisi dati\\Functions\\")
sys.path.append("D:\\Academic Research\\Analisi dati\\Functions\\")
from MyFunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
mpl.rcParams['figure.dpi'] = 300             # Per evitare che i plot inline siamo smearati

# =========================================================================================
#                                      DEFINIZIONI                                      
# =========================================================================================
# Definisco le dimensioni dei font e delle figure e altre variabili generali
textfont = 25         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.png'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filedpi = 540         # Risoluzione in caso di salvataggio di png/jpeg
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                                  DATI DALLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = 'statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Estraggo tutte le variabili di interesse dal file di status
ifVM = statusdata['ifVM']
fileprename = statusdata['fileprename']
n_jobs = statusdata['n_jobs']
imgloc = statusdata['imgloc']
temploc = statusdata['temploc']

# ================================================================================
#              INFORMAZIONI PRELIMINARI SULL'ESTRAZIONE DATI
# ================================================================================
# Definisco il fascio da usare
beamtype = 'unif_g251_unif_h501point10cm3'    # cristalli da 1x1x1 cm^3

# Definisco le configurazioni esistenti
crystalconfig = ['axial','amorphous']
particles = ['gamma','neutron']
crystalcolors = ['mediumblue','red']

# Definisco la codifica di neutroni e fotoni
idvalues = [1,0]   # Assegno lo zero ai gamma e l'uno ai neutroni

# Definisco la struttura del setup sperimentale
crystalDepthOrientedTotal = 4       # Dimension along z (oriented) [cm]
crystalDepthRandomTotal = 12        # Dimension along z (non oriented) [cm]
crystalLatTotal = 5                 # Dimension along x and y (both types) [cm]

if beamtype == 'unif_g251_unif_h501':
	# Fascio incidente su cristalli da 1x1x4 cm^3, con FWHM = 3.5 cm e divergenza 0.1 mrad
	crystalDepth = 4                # Dimension along z [cm]
	crystalLatSingle = 1            # Dimension along x and y [cm]

elif beamtype == 'unif_g251_unif_h501point10cm3':
	# Fascio incidente su cristalli da 1x1x1 cm^3, con FWHM e divergenza nulla
	crystalDepth = 1                # Dimension along z [cm]
	crystalLatSingle = 1            # Dimension along x and y [cm]
else:
	# Warning
	print(f"Beam type not recognized!\n")

NumCrystalsDepthOriented = int(crystalDepthOrientedTotal / crystalDepth) # Number of crystals along z (oriented)
NumCrystalsDepthRandom = int(crystalDepthRandomTotal / crystalDepth)     # Number of crystals along z (non oriented)
NumCrystalsDepth = NumCrystalsDepthOriented + NumCrystalsDepthRandom     # Number of crystals along z (both types)
NumCrystalsLatSingle = int(crystalLatTotal / crystalLatSingle)           # Number of crystals along x and y (both types)
NumCrystalsTotal = NumCrystalsDepth * NumCrystalsLatSingle**2            # Total number of crystals in the whole crystal
crystalOffset = (NumCrystalsLatSingle - 1)/2                             # Offset between the center of the 00 crystal and the center of the face...

# Nel nome delle key dei dati salvati, qual è l'indice (della stringa)
# che indica il layer di appartenenza?
layerid = 10

# Criteri di selezione dell'evento
#Emean = 150
Emean = 120
Ewidth = 0.5

# Che evento guardo?
IndEventToBeExtracted = 0

# Normalizzo l'energia depositata per layer?
ifnorma = False

# ================================================================================
#              INFORMAZIONI PRELIMINARI SULL'APERTURA FILE
# ================================================================================
# Quanti eventi carico in memoria? (Faccio un bunch unico)
NumEventsPerBunch = 10000

# ================================================================================
#							ESTRAZIONE DEI DATI DAL FILE
# ================================================================================
# Itero su tutte le combinazioni particella/cristallo.
outputdata = []
print(f"Beginning the data extraction...\n")

for i,eli in enumerate(crystalconfig):
	for j,elj in enumerate(particles):
		# ================================================================================
		#                            ESTRAZIONE DATI
		# ================================================================================
		# Definisco il nome del file da caricare
		filepostname = 'tbeamdata_' + eli + '_' + elj + '_' + beamtype + '.root'
		print(f"Crystal in {eli} alignment, {elj} beam")
		print(f"Filename: {filepostname}")
		
		# Apro il file e calcolo il numero di eventi a disposizione
		inputfile = uproot.open(fileprename + filepostname)['outData']

		# ================================================================================
		#                            BUNCHING
		# ================================================================================
		# Preinizializzo le matrici da riempire con i dati del file
		# Nota sul numero di colonne: ne ho una per ciascun cristallo nel calorimetro, 
		# più una per ogni layer lungo z, più una per l'intero calorimetro, più una per
		# la classe
		midoutputdata = np.zeros((NumEventsPerBunch,NumCrystalsTotal+NumCrystalsDepth+2),
																	   dtype=np.float32)
		partial_energies = np.zeros((NumEventsPerBunch,NumCrystalsDepth+1),dtype=np.float32)
		
		# Estraggo le energie depositate in ogni cristallo evento per evento.
		# I dati estratti finiscono in una matrice che ha sei colonne extra: 
		#	- Classe (fotoni = 1, neutroni = 0)
		#	- Frazione di energia depositata in ogni layer in z (integrando in x e y)
		# 	  normalizzata all'energia totale
		#	- Energia totale depositata nel calorimetro
		
		for k,elk in enumerate(inputfile.keys()):
			# Estraggo e salvo l'energia depositata in un singolo cristallino.
			# Se sto guardando la key "numero di evento", inserisco al suo posto 
			# l'identificatore di particella, mentre salto i seed.
			if elk == 'NEvent':
				midoutputdata[:,k] = idvalues[j]
			elif (elk == 'Seed0') | (elk == 'Seed1'):
				continue
			else:
				# Estraggo l'energia del layer.
				energy_crys = np.array(inputfile[elk].array(entry_start = 0,
															entry_stop = NumEventsPerBunch),
					                                        dtype = np.float32)

				# Salvo le energie nella colonna corretta
				midoutputdata[:,k] = energy_crys
				
				# Aggiorno le energie parziali di ogni layer
				layerind = int(str(elk).split('_')[2][1:])
				partial_energies[:,layerind] += energy_crys   # Aumento l'energia del layer parziale
				partial_energies[:,-1] += energy_crys         # Aumento l'energia totale

		# Normalizzo le energie depositate per layer al totale, se richiesto
		if ifnorma:
			for normind in range(NumCrystalsDepth):
				partial_energies[:,normind] = partial_energies[:,normind] / partial_energies[:,-1]

		# A fine ciclo, inserisco le energie parziali nel dataset totale e chiudo il file
		inputfile.close()
		midoutputdata[:,-(NumCrystalsDepth+1):] = partial_energies

		# ================================================================================
		#                            SELEZIONE DEGLI EVENTI
		# ================================================================================
		# Seleziono un evento di neutroni o di fotoni che sia dell'energia depositata richiesta
		# e dell'energia iniziale richieste
		cond = (midoutputdata[:,0] == idvalues[j]) & \
			   (midoutputdata[:,-1] >= Emean - Ewidth) & (midoutputdata[:,-1] <= Emean + Ewidth)
		midoutputdata_out = midoutputdata[cond,:][IndEventToBeExtracted,:]

		# Lo salvo
		outputdata.append(midoutputdata_out)
		print(f"Done! \n")

print(f"Extraction done!\n")

# ================================================================================
#              PLOT IN 3D DI CUBI INTORNO AI CRISTALLI
# ================================================================================
# Preparo il plot del cubo 3D. Per farlo, mi serve una funzione ad hoc
# presa dall'esempio svolto "voxels numpy logo" vedi:
# https://matplotlib.org/stable/api/_as_gen/mpl_toolkits.mplot3d.axes3d.Axes3D.voxels.html
def explode(data):
    size = np.array(data.shape)*2
    data_e = np.zeros(size - 1, dtype=data.dtype)
    data_e[::2, ::2, ::2] = data
    return data_e

# Definisco le facce da colorare IN ORIENTATO
filled_cubeOR = np.ones((1, 1, 1),)
filled_cubeOR_exploded = explode(filled_cubeOR)

# Definisco i vertici da disegnare
xminOR = yminOR = -(crystalLatTotal/2)
xmaxOR = ymaxOR = (crystalLatTotal/2)
zminOR = 0
zmaxOR = crystalDepthOrientedTotal

x_cubeOR = np.array([[[xminOR, xminOR], [xminOR, xminOR]],
				     [[xmaxOR, xmaxOR], [xmaxOR, xmaxOR]]])

y_cubeOR = np.array([[[yminOR, yminOR], [ymaxOR, ymaxOR]],
				     [[yminOR, yminOR], [ymaxOR, ymaxOR]]])

z_cubeOR = np.array([[[zminOR, zmaxOR], [zminOR, zmaxOR]],
				     [[zminOR, zmaxOR], [zminOR, zmaxOR]]])


# ================================================================================
# Definisco le facce da colorare IN AMORFO
filled_cubeAM = np.ones((1, 1, 1),)
filled_cubeAM_exploded = explode(filled_cubeAM)

# Definisco i vertici da disegnare
xminAM = yminAM = -(crystalLatTotal/2)
xmaxAM = ymaxAM = (crystalLatTotal/2)
zminAM = crystalDepthOrientedTotal
zmaxAM = crystalDepthOrientedTotal + crystalDepthRandomTotal

x_cubeAM = np.array([[[xminAM, xminAM], [xminAM, xminAM]],
				     [[xmaxAM, xmaxAM], [xmaxAM, xmaxAM]]])

y_cubeAM = np.array([[[yminAM, yminAM], [ymaxAM, ymaxAM]],
				     [[yminAM, yminAM], [ymaxAM, ymaxAM]]])

z_cubeAM = np.array([[[zminAM, zmaxAM], [zminAM, zmaxAM]],
				     [[zminAM, zmaxAM], [zminAM, zmaxAM]]])

# ================================================================================
# Definisco le facce da colorare IN AMORFO
filled_cubeAMTOT = np.ones((1, 1, 1),)
filled_cubeAMTOT_exploded = explode(filled_cubeAMTOT)

# Definisco i vertici da disegnare
xminAMTOT = yminAMTOT = -(crystalLatTotal/2)
xmaxAMTOT = ymaxAMTOT = (crystalLatTotal/2)
zminAMTOT = 0
zmaxAMTOT = crystalDepthOrientedTotal + crystalDepthRandomTotal

x_cubeAMTOT = np.array([[[xminAMTOT, xminAMTOT], [xminAMTOT, xminAMTOT]],
				    	[[xmaxAMTOT, xmaxAMTOT], [xmaxAMTOT, xmaxAMTOT]]])

y_cubeAMTOT = np.array([[[yminAMTOT, yminAMTOT], [ymaxAMTOT, ymaxAMTOT]],
				    	[[yminAMTOT, yminAMTOT], [ymaxAMTOT, ymaxAMTOT]]])

z_cubeAMTOT = np.array([[[zminAMTOT, zmaxAMTOT], [zminAMTOT, zmaxAMTOT]],
				    	[[zminAMTOT, zmaxAMTOT], [zminAMTOT, zmaxAMTOT]]])

# ================================================================================
#              PLOT IN 3D DI UN EVENTO DI SCIAME EM
# ================================================================================
# Definisco se usare una scala colore in log o in lineare
try:
	if (sys.argv[1] == 'log'):
		iflog = True
	else:
		iflog = False
except:
	iflog = True 

# Definisco altre variabili utili per la grafica
cmap = 'gnuplot2_r'            # Mappa colore per i plot in 3D
ifTwoNorm = False              # Se la scala colore non è in log, uso un TwoNorm?
ind0 = 1                       # Di ogni evento considero solo i valori in outputdata[...] da ind0 a ind1
ind1 = NumCrystalsDepth + 1    # Con [1,NumCrystalsDepth+1) guardo solo le energie depositate nei cristalli

# Creo la cmap
mycmap = eval('copy.copy(pltmaps.' + cmap + ')')
mycmap.set_bad(mycmap(0))

# Definisco una stringa utile per i plot
if iflog:
	logstr = "log"
else:
	logstr = "lin"

# Definisco il punto di impatto della particella
x0 = 0
y0 = 0
z0 = -1

# Preparo il plot di due sciami e.m.
all_xs = np.zeros((NumCrystalsTotal,))  # x,y,z del centro di ogni cristallo
all_ys = np.zeros_like(all_xs)
all_zs = np.zeros_like(all_xs)

for i in range(NumCrystalsTotal):
	# Definisco la x e la y del vertice i-esimo del cristallo (per ogni bunch da 25, le 
	# x e le y fanno una matrice 5x5)
	ind_row, ind_col = divmod(i % (NumCrystalsLatSingle**2),NumCrystalsLatSingle)
	all_xs[i] = (ind_col - crystalOffset) * crystalLatSingle
	all_ys[i] = (ind_row - crystalOffset) * crystalLatSingle

	# Definisco la z del vertice i-esimo del cristallo
	all_zs[i] = (i // NumCrystalsLatSingle**2) * crystalDepth

# Procedo con il plot
print(f"Plotting...")
Ethr = 120
alltitles = [[r"Axial, $\gamma$", r"Axial, $n$"],[r"Random, $\gamma$", r"Random, $n$"]]

for i,eli in enumerate(crystalconfig):
	for j,elj in enumerate(particles):
		# Creo una figura per ogni particella e configurazione
		fig = plt.figure(figsize = (25,20))
		ax = fig.add_subplot(projection='3d')
		#ax.view_init(elev=3, azim=55, roll=None)
		#ax.view_init(elev=5, azim=83, roll=None) #80 good
		ax.view_init(elev=4, azim=83, roll=None) #80 good

		# Mostro il punto di impatto in nero
		ax.scatter(x0,y0,z0,color='black',marker='^',s=400)
		ax.plot3D([x0,x0],[y0,y0],[z0,zmaxAM],color='black',linestyle=':',linewidth=3)

		# Aggiungo il plot del cubo 3D
		if eli == 'axial':
			ax.voxels(x_cubeOR, y_cubeOR, z_cubeOR, filled_cubeOR_exploded, 
					  facecolors='grey', edgecolors='darkturquoise',
					  alpha=0,label = f"Oriented", linewidth=2, linestyle='-',shade=False)
			ax.voxels(x_cubeAM, y_cubeAM, z_cubeAM, filled_cubeAM_exploded,
					  facecolors='grey', edgecolors='darkorange',
					  alpha=0,label = f"Random", linewidth=2, linestyle='-',shade=False)
		else:
			ax.voxels(x_cubeAMTOT, y_cubeAMTOT, z_cubeAMTOT, filled_cubeAMTOT_exploded,
					  facecolors='grey', edgecolors='darkorange',
					  alpha=0,label = f"Random", linewidth=2, linestyle='-',shade=False)

		# Mostro l'energia depositata in ogni torre del calorimetro
		all_cs = outputdata[i*len(crystalconfig)+j][ind0:-ind1]
		if iflog:
			vmin = 1*10**(-1)    # Good for 100, 150 GeV
			vmax = 1*10**(1)
			thisnorm = colors.LogNorm(vmin=vmin,vmax=vmax)
		else:
			if Emean <= Ethr:
				vmin = 0.1               # Good for 50-100 GeV prev 0
				vmax = 10
				vmid = 0.5
			else:
				vmin = 0
				vmax = 17.5              # Good for 150 GeV
				vmid = Emean/500
			if ifTwoNorm:
				thisnorm = colors.TwoSlopeNorm(vmid,vmin=vmin,vmax=vmax)
			else:
				thisnorm = colors.Normalize(vmin=vmin,vmax=vmax)
		
		p = ax.scatter(all_xs,all_ys,all_zs,c=all_cs,cmap=mycmap,marker='.',s=700,alpha=1,norm=thisnorm)

		print(f"vmin: {vmin}; vmax: {vmax}")
		print(f"min Edep: {np.min(all_cs[all_cs>0]):.4e}; max Edep: {np.max(all_cs):.4e}\n")

		# Rifinisco la grafica
		ax.set_xlabel('\nx [cm]',fontsize = 1.5*textfont, linespacing=3.5)
		ax.set_ylabel('\ny [cm]',fontsize = 1.5*textfont, linespacing=3.5)
		ax.set_zlabel('\nz [cm]',fontsize = 1.5*textfont, linespacing=5,rotation = 270)
		ax.set_xticks(list(np.unique(all_xs)),[str(el) for el in np.unique(all_xs)],fontsize=textfont*1.25)
		ax.set_yticks(list(np.unique(all_ys)[::2]),[str(el) for el in np.unique(all_ys)[::2]],fontsize=textfont*1.25)
		ax.set_zticks(list(np.unique(all_zs)[::2]),[str(el) for el in np.unique(all_zs)[::2]],fontsize=textfont*1.25)
		ax.tick_params(labelsize = textfont)
		ax.tick_params(axis = 'y', pad = 15)
		ax.tick_params(axis = 'z', pad = 25)
		ax.xaxis.labelpad = 5
		ax.yaxis.labelpad = 5
		ax.zaxis.labelpad = 5
		cb = fig.colorbar(p,shrink=0.5,pad=0)
		cb.set_label(r'E$_{dep}$ [GeV]',fontsize=textfont*1.25,rotation=270,labelpad=50)
		for elk in cb.ax.get_yticklabels():
			elk.set_fontsize(textfont*1.25)
		
		# Salvo la figura
		#fig.suptitle(f'{eli}, {elj}, {all_cs.sum():.2f} GeV',x=0.5,y=0.75,fontsize=textfont*2.5)
		fig.suptitle(alltitles[i][j],x=0.5,y=0.75,fontsize=textfont*2.5)
		#fig.set_tight_layout('tight')
		#fig.tight_layout(rect=[0, 0, 0.99, 0.99])
		fig.savefig(os.path.join(imgloc,f"pubplots_shower3D{logstr}_{eli}_{elj}.pdf"),
					dpi=filedpi,bbox_inches='tight',pad_inches = 0.7)
		#plt.show()
		plt.close(fig)

print(f"Plotting done!\n")

# ================================================================================
#                       CROPPING DELLE IMMAGINI
# ================================================================================
# Definisco come tagliare le immagini
x, y, w, h = (100, 200, 1400, 825)

# Ritaglio le immagini prodotte
print(f"Cropping the figures...")

for i,eli in enumerate(crystalconfig):
	for j,elj in enumerate(particles):
		# Eseguo il cropping. Codice da: https://stackoverflow.com/questions/457207/cropping-pages-of-a-pdf-file
		ijfile = os.path.join(imgloc,f"pubplots_shower3D{logstr}_{eli}_{elj}.pdf")
		ijoutfile = os.path.join(imgloc,f"pubplots_cropped_shower3D{logstr}_{eli}_{elj}.pdf")

		with open(ijfile, "rb") as inputPDF:
		    # Apro il file di input e di output
		    ReaderFile = PyPDF2.PdfFileReader(inputPDF)
		    WriterFile = PyPDF2.PdfFileWriter()

		    # Definisco la struttura croppata
		    page_x, page_y = ReaderFile.getPage(0).cropBox.getUpperLeft()
		    upperLeft = [page_x.as_numeric(), page_y.as_numeric()]
		    new_upperLeft  = (upperLeft[0] + x, upperLeft[1] - y)
		    new_lowerRight = (new_upperLeft[0] + w, new_upperLeft[1] - h)

		    # Applico la struttura croppata all'unica pagina disponibile (la 0)
		    page = ReaderFile.getPage(0)
		    page.cropBox.upperLeft  = new_upperLeft
		    page.cropBox.lowerRight = new_lowerRight
		    WriterFile.addPage(page)

		    # Scrivo effettivamente il contenuto nel file
		    with open(ijoutfile, "wb") as out_f:
		        WriterFile.write(out_f)

		# Rimuovo la figura originale
		print(f"Removing {ijfile} ...")
		os.remove(ijfile)

print(f"Figures cropped!\n")

# ================================================================================
#                       MERGING DELLE IMMAGINI
# ================================================================================
# Merge following https://stackoverflow.com/questions/56970605/how-to-merge-two-pages-from-a-pdf-file-as-one-page
print(f"Merging the figures...")

# Open the files that have to be merged
pdf1File = open(os.path.join(imgloc,f'pubplots_cropped_shower3D{logstr}_amorphous_neutron.pdf'), 'rb')
pdf2File = open(os.path.join(imgloc,f'pubplots_cropped_shower3D{logstr}_amorphous_gamma.pdf'), 'rb')
pdf3File = open(os.path.join(imgloc,f'pubplots_cropped_shower3D{logstr}_axial_gamma.pdf'), 'rb')

# Read the files that you have opened
pdf1Reader = PyPDF2.PdfFileReader(pdf1File)
pdf2Reader = PyPDF2.PdfFileReader(pdf2File)
pdf3Reader = PyPDF2.PdfFileReader(pdf3File)

# Make a list of all pages
pages = []
for Reader in [pdf1Reader,pdf2Reader,pdf3Reader]:
	for pageNum in range(Reader.numPages):
	    pageObj = Reader.getPage(pageNum)
	    pages.append(pageObj)

# Calculate width and height for final output page
width = pages[1].mediaBox.getWidth() * 3
height = pages[1].mediaBox.getHeight() 

# Create blank page to merge all pages in one page
merged_page = PyPDF2.PageObject.createBlankPage(None, width, height)
writer = PyPDF2.PdfFileWriter()

# Loop through all pages and merge / add them to blank page
x = 0
for page in pages:
    merged_page.mergeScaledTranslatedPage(page, 1, x, 10)
    x = float(x) + float(page.mediaBox.getWidth())

# Create final file with one page
writer = PyPDF2.PdfFileWriter()
writer.addPage(merged_page)

with open(os.path.join(imgloc,f'pubplots_allshowers_{logstr}.pdf'), 'wb') as f:
    writer.write(f)

print(f"Merging completed! \n")
print(f"Job done!\n")