# ==========================================================================================
#                                  PREMESSA (sintassi)
# ==========================================================================================
# Questo script acquisisce in input quattro file dati raw generati via Geant4, contenenti
# i risultati delle simulazioni eseguite con due tipo di particelle diverse (fotoni, neutroni)
# e in due condizioni diverse (random, asse). Lo script estrae gli eventi e produce alcune
# mappe di correlazione, utili per comprendere meccanismi manuali di classificazione.
# Di fatti, si correlano Etot con EL0 (raw o normalizzata) e con EL1 (raw o normalizzata).
# Si possono anche applicare tagli su Ein, se disponibile.
#
# Sintassi di call:
#   	python pubplots_ELXvsEdep_scatter.py
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import matplotlib.pyplot as plt        # Per ambienti di plot
import matplotlib.colors as colors     # Per i colori dei plot
import scipy.io as io                  # Per particolari funzioni di I/O
import scipy.signal as sig             # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg           # Per analisi dei segnali e delle immagini
import os                              # Per agire sul sistema operativo
import sys                             # [come sopra]
import numpy as np                     # Per analisi numeriche
import pandas as pd                    # Per analisi numeriche con i dataframe
import time                            # Per il timing degli script
import gc                              # Per la pulizia della memoria
import uproot                          # Per l'upload dei file root
import lmfit                           # Per fit avazati
import json                            # Per leggere i file JSON
import PyPDF2                          # Per unire i PDF alla fine
import glob                            # Per manipolazione dei path
import h5py                            # Per caricare i file .h5
import copy                            # Per copiare gli oggetti in modo corretto
import subprocess                      # Per chiamare script come da bash
import matplotlib as mpl               # Per rendering dei plot
from sklearn import utils as sk_utils  # Per AI

# Import delle funzioni personali, localizzate nella cartella di root delle analisi
sys.path.append("/eos/user/p/pmontigu/Functions/")
sys.path.append("C:\\Users\\Pietro\\Desktop\\Academic Research\\Analisi dati\\Functions\\")
sys.path.append("D:\\Academic Research\\Analisi dati\\Functions\\")
from MyFunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
mpl.rcParams['figure.dpi'] = 300             # Per evitare che i plot inline siamo smearati

# =========================================================================================
#                                      DEFINIZIONI                                      
# =========================================================================================
# Definisco le dimensioni dei font e delle figure e altre variabili generali
textfont = 20         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.pdf'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filedpi = 320         # Risoluzione in caso di salvataggio di png/jpeg
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                                  DATI DALLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = 'statusfile.json'
with open(statusfile) as json_file:
	statusdata = json.load(json_file)

# Estraggo tutte le variabili di interesse dal file di status
ifVM = statusdata['ifVM']
fileprename = statusdata['fileprename']
n_jobs = statusdata['n_jobs']
imgloc = statusdata['imgloc']
temploc = statusdata['temploc']

# ================================================================================
#                                 VARIABILI GENERALI
# ================================================================================
# Definisco il fascio da usare
beamtype = 'unif_g251_unif_h501'         # Physics run at equal Edep
#beamtype = 'unif_g201_unif_h201'         # Physics run at equal Ein
#beamtype = 'unif1_251unif1_251large'      # Old run

# Definisco le condizioni di cristallo da analizzare
crystalconds = ['ax','am']
all_titles = ['Random alignment','Axial alignment']           # Questi sono i titoli dei plot!

# Deduco dal tipo di fascio se in questo file è salvata anche l'energia iniziale (Ein)
if (beamtype == 'unif_g251_unif_h501') | (beamtype == 'unif1_251unif1_251large'):
	ifEin = False
elif (beamtype == 'unif_g201_unif_h201'):
	ifEin = True

# Dimensione dei marker nei plot
dimsize = 10

# Definisco gli indici di colonna dove si trovano le energie di layer.
# N.B. questi sono gli stessi per i dataset con e senza Ein, perché tanto
# le variabili "data_a*_*" sono costruite prendendo solo le energie dei 
# layer e l'energia totale
ind_EL0 = 0
ind_EL1 = 1
ind_EL2 = 2
ind_EL3 = 3
ind_Etot = -1

# ==========================================================================================
#                                      DEFINIZIONE COLORI
# ==========================================================================================
# Costruisco colori per gli scatter plot che siano BW compliant
gnuplot_cmap = plt.get_cmap('gnuplot2')
indices = np.linspace(0, gnuplot_cmap.N, 20)
my_colors = [gnuplot_cmap(int(i)) for i in indices]
crystalcondcolors = [my_colors[4],my_colors[13]] # Blu, Rosso

# ================================================================================
#                                 ESTRAZIONE DATI
# ================================================================================
# Apro i file per entrambe le condizioni e ne estraggo i dati
outputdata = []
outputclasses = []
if ifEin:
	outputEin = []

forcedEmin = 26             # Cut inferiore in energia

print(f"")
print(f"Beginning the data extraction...\n")

for i,el in enumerate(crystalconds):
	# Definisco il nome del file dati
	filename = fileprename + 'eqmerged_data_' + beamtype + '.npz'
	with np.load(filename) as dataset:
	    # Creo un dataframe a partire dal dataset
	    df = pd.DataFrame(dataset['data_' + el],dtype=np.float32)

	    # Creo la condizione di taglio in energia: devo decidere se totale o incidente
	    if ifEin:
	    	Etobecut = np.array(df.iloc[:,-6:])
	    else:
	    	Etobecut = np.array(df.iloc[:,-1])

	    cutcond = (Etobecut >= forcedEmin)

	    # Estraggo le energie parziali e totali dal dataframe
	    partial_energies = np.array(df.iloc[cutcond,-5:])

	    # Se le energie parziali sono normalizzate, le moltplico per Edep
	    # in modo da poterne usare il valore in GeV in questo script
	    ifDataAreNormalized = dataset['ifnorma']
	    if ifDataAreNormalized:
	    	for j in range(4):
	    		partial_energies[:,j] = partial_energies[:,j] * partial_energies[:,-1]
	    
	    outputdata.append(partial_energies)

	    # Se esistono, estraggo le energie iniziali
	    if ifEin:
	    	outputEin.append(np.array(df.iloc[cutcond,-6]))

	    # Estraggo le classi
	    y = np.int16(df.iloc[cutcond,0])
	    outputclasses.append(y)

print(f"Extraction done!\n")

# ================================================================================
#                                 SELEZIONE DATI
# ================================================================================
# Separo i dati in amorfo e assiale, gamma e neutrone secondo la codifica di tutti
# i file: g = 1, n = 0
cond_ax_gamma = (outputclasses[0] > 0)
cond_ax_neutron = (cond_ax_gamma == False)
cond_am_gamma = (outputclasses[1] > 0)
cond_am_neutron = (cond_am_gamma == False)

data_ax_gamma = outputdata[0][cond_ax_gamma,:]
data_ax_neutron = outputdata[0][cond_ax_neutron,:]
data_am_gamma = outputdata[1][cond_am_gamma,:]
data_am_neutron = outputdata[1][cond_am_neutron,:]

# Se esiste, estraggo Ein
if ifEin:
	Ein_ax_gamma = outputEin[0][cond_ax_gamma]
	Ein_ax_neutron = outputEin[0][cond_ax_neutron]
	Ein_am_gamma = outputEin[1][cond_am_gamma]
	Ein_am_neutron = outputEin[1][cond_am_neutron]

# ================================================================================
#                                CUT IN ENERGIA INIZIALE
# ================================================================================
# Indipendentemente dal taglio, compatto i dati in due liste per rendere 
# più facile l'iterazione su di esse
data_gamma = [data_am_gamma,data_ax_gamma]
data_neutron = [data_am_neutron,data_ax_neutron]
if ifEin:
	if ifEcut:
		Ein_am_gamma = Ein_am_gamma[cond_am_gamma]
		Ein_ax_gamma = Ein_ax_gamma[cond_ax_gamma]
		Ein_am_neutron = Ein_am_neutron[cond_am_neutron]
		Ein_ax_neutron = Ein_ax_neutron[cond_ax_neutron]

	Ein_gamma = [Ein_am_gamma,Ein_ax_gamma]
	Ein_neutron = [Ein_am_neutron,Ein_ax_neutron]

# ================================================================================
#                                HISTO1D of EL0 + EL1
# ================================================================================
# Produco un istogramma di energia depositata in L0
nbins = int((106-26)/2)
xlims = [26,106]
thr1 = 48.5
thr2 = 51

fig = plt.figure(figsize = dimfig)
for i in range(2):
	# Estraggo le energie
	Egamma = data_gamma[i][:,ind_EL0] + data_gamma[i][:,ind_EL1]
	Eneutron = data_neutron[i][:,ind_EL0] + data_neutron[i][:,ind_EL1]

	# Produco gli istogrammi e li normalizzo
	Ebins = np.linspace(xlims[0],xlims[1],nbins)
	tEbins = truebins(Ebins)

	histo_gamma, _ = truehisto1D(Egamma, bins=Ebins)
	histo_neutron, _ = truehisto1D(Eneutron, bins=Ebins)

	# Faccio due conti
	boolgamma = (Egamma >= thr1) & (Egamma < thr2)
	boolneutron = (Eneutron >= thr1) & (Eneutron < thr2)
	numgamma = np.sum(boolgamma)
	numneutron = np.sum(boolneutron)
	print(f"Condizione {i}")
	print(f"Numero di gamma tra {thr1} e {thr2}: {numgamma}")
	print(f"Numero di neutroni tra {thr1} e {thr2}: {numneutron}")

	# Produco il plot
	ax = fig.add_subplot(1,2,i+1)
	histoplotter1D(ax,tEbins,histo_gamma,leglabel = r'$\gamma$', hcolor = crystalcondcolors[0])
	histoplotter1D(ax,tEbins,histo_neutron,leglabel = r'$n$', hcolor = crystalcondcolors[1])
	ax.plot(tEbins,tEbins*0+10000,color='black',linestyle='--',label='10000 counts')
	ax.plot(tEbins,tEbins*0+7500,color='limegreen',linestyle='--',label='7500 counts')
	#ax.plot(tEbins,tEbins*0+5000,color='darkgreen',linestyle='--',label='5000 counts')
	ax.set_xlabel(r'$E_{L,1} + E_{L,2}$',fontsize = textfont)
	ax.set_ylabel('Counts per bin',fontsize = textfont)
	#ax.set_yscale('log')
	ax.legend(loc='best',fontsize=textfont*0.75,framealpha=1)
	ax.grid(linewidth=0.5)
	ax.set_title(all_titles[i],fontsize=textfont)

fig.set_tight_layout('tight')
filename = f"pubplots_histo1D_Edep_onlyL1L2" + filetype
fig.savefig(os.path.join(imgloc,f"{beamtype}",filename),dpi=filedpi)
#plt.show()
plt.close(fig)

print(f"Histo1D EL0 + EL1: done!\n")

# ================================================================================
#                                HISTO1D of Etot
# ================================================================================
# Produco un istogramma di energia depositata in totale
nbins = 50

fig = plt.figure(figsize = dimfig)
for i in range(2):
	# Produco gli istogrammi e li normalizzo
	xlims = [26,151]
	Ebins = np.linspace(xlims[0],xlims[-1],nbins)
	tEbins = truebins(Ebins)

	histo_gamma, _ = truehisto1D(data_gamma[i][:,ind_Etot], bins=Ebins)
	histo_neutron, _ = truehisto1D(data_neutron[i][:,ind_Etot], bins=Ebins)

	# Produco il plot
	ax = fig.add_subplot(1,2,i+1)
	histoplotter1D(ax,tEbins,histo_gamma,leglabel = r'$\gamma$', hcolor = crystalcondcolors[0])
	histoplotter1D(ax,tEbins,histo_neutron,leglabel = r'$n$', hcolor = crystalcondcolors[1])
	ax.plot(tEbins,tEbins*0+10000,color='black',linestyle='--')
	ax.set_xlabel('Total energy deposited [GeV]',fontsize = textfont)
	ax.set_ylabel('Normalized counts',fontsize = textfont)
	ax.set_ylim([9800,10500])
	#ax.set_yscale('log')
	ax.legend(loc='best',fontsize=textfont,framealpha=1)
	ax.grid(linewidth=0.5)
	ax.set_title(all_titles[i],fontsize=textfont)

fig.set_tight_layout('tight')
filename = f"pubplots_histo1D_Etot" + filetype
fig.savefig(os.path.join(imgloc,f"{beamtype}",filename),dpi=filedpi)
#plt.show()
plt.close(fig)

print(f"Histo1D Etot: done!\n")
print(f"Job done!\n")