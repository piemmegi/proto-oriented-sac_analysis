# ==========================================================================================
#                                  PREMESSA (sintassi)
# ==========================================================================================
# Questo script analizza i dati di Grid Search prodotti d "classifier_gridsearch" e mostra
# come varia l'accuracy di classificazione in funzione del valore di un iperparametro (o due),
# tenendo fissati tutti gli altri al loro valore ottimale.
#
# Sintassi di call:
#   python classifier_gridsearch.py [classifier] [orientation] [beamtype] [parind_1] [parind_2]
#
# Esempio di call:
# $ conda activate pypmg
# $ cd "C:\Users\Pietro\Desktop\Academic Research\Analisi dati\Analysis_projects\2023\Simulazioni Cristalli Orientati\proto-oriented-sac_analysis"
# $ python classifier_gridseachreport.py rf am unif_g251_unif_h501 0 2
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import di moduli interni
import matplotlib.pyplot as plt      # Per ambienti di plot
import matplotlib.colors as colors   # Per i colori dei plot
import scipy.io as io                # Per particolari funzioni di I/O
import scipy.signal as sig           # Per analisi dei segnali e delle waveform
import scipy.ndimage as ndmg         # Per analisi dei segnali e delle immagini
import os                            # Per agire sul sistema operativo
import sys                           # [come sopra]
import numpy as np                   # Per analisi numeriche
import pandas as pd                  # Per analisi numeriche con i dataframe
import time                          # Per il timing degli script
import gc                            # Per la pulizia della memoria
import uproot                        # Per l'upload dei file root
import lmfit                         # Per fit avazati
import json                          # Per leggere i file JSON
import PyPDF2                        # Per unire i PDF alla fine
import glob                          # Per manipolazione dei path
import h5py                          # Per caricare i file .h5
import copy                          # Per copiare gli oggetti in modo corretto
import subprocess                    # Per chiamare script come da bash
import matplotlib as mpl             # Per rendering dei plot
import inspect                       # Per ispezione dei codici
import warnings                      # Per disattivare i Deprecation Warning
import seaborn                       # Per plot aesthetic
from sklearn import preprocessing    # ML / preprocessing
from sklearn import model_selection  # ML / divisione in TrS e TeS
from sklearn import decomposition    # ML / PCA
from sklearn import neighbors        # ML / KNN
from sklearn import metrics          # ML / Metriche di scoring
from sklearn import ensemble         # ML / Random forests

# Import di funzioni singole o funzioni personali, dal file "MyFunctions.py"
sys.path.append("C:\\Users\\Pietro\\Desktop\\Academic Research\\Analisi dati\\Functions\\")
sys.path.append("D:\\Academic Research\\Analisi dati\\Functions\\")
from MyFunctions import *

# Set dei flag dei pacchetti
np.seterr(divide='ignore', invalid='ignore') # Per evitare print di errori in caso di divisioni 0/0
mpl.rcParams['figure.dpi'] = 300             # Per evitare che i plot inline siamo smearati
warnings.filterwarnings("ignore", category=DeprecationWarning)

# =========================================================================================
#                                      DEFINIZIONI                                      
# =========================================================================================
# Definisco le dimensioni dei font e delle figure e altre variabili generali
textfont = 20         # Fontsize per label e legend
dimfig = (12,7)       # Dimensioni delle figure (simile allo standard A4)
dimfigbig = (16,12)   # Dimensioni delle figure (più grande di A4, per massima leggibilità)
dimfiglong = (16,25)  # Dimensioni delle figure (formato lungo, per molti plot, tipo 11x2)
filetype = '.png'     # Formato di salvataggio delle immagini (jpeg per le presentazioni, pdf per la tesi)
filedpi = 540         # Risoluzione in caso di salvataggio di png/jpeg
nptsfit = 100000      # Numero di punti per il plot dei fit

# ==========================================================================================
#                                  DATI DALLO STATUSFILE
# ==========================================================================================
# Leggo il file di status
statusfile = 'statusfile.json'
with open(statusfile) as json_file:
    statusdata = json.load(json_file)
    
# Estraggo tutte le variabili di interesse dal file di status
ifVM = statusdata['ifVM']
fileprename = statusdata['fileprename']
n_jobs = statusdata['n_jobs']
random_state = statusdata['random_state']
imgloc = statusdata['imgloc']
temploc = statusdata['temploc']

# ==============================================================================
#                                LOAD DEI DATI
# ==============================================================================
# Definisco il classificatore
classifiertype = sys.argv[1]

# Definisco se sono in amorfo o in assiale
crystalcond = sys.argv[2]

# Definisco il tipo di fascio da analizzare
beamtype = sys.argv[3]

# Definisco l'indice degli iperparametri da fissare
parind_1 = int(sys.argv[4])
parind_2 = int(sys.argv[5])

# Definisco la convezione indice-tipo di variabile
if (classifiertype == 'rf'):
    conversion_list = ["n_estimators", "criterion", "max_depth", "max_features"]
elif (classifiertype == 'nn'):
    conversion_list = ["n_neurons", "activation", "solver", "alpha", "learning_rate_init", "max_iter"]

print(f"Beginning the GS analysis...")
print(f"Hyperarameter 1 to be studied: {conversion_list[parind_1]} (n. {parind_1})")
print(f"Hyperarameter 2 to be studied: {conversion_list[parind_2]} (n. {parind_2})")
print(f"")

# ==============================================================================
#                                FUNZIONE CUSTOM
# ==============================================================================
def OutputCaster(maxfile,classifiertype='rf'):
    # Considero "maxfile" come nome di un file di output. Lo strippo, in base
    # a codifiche ben definite, per ricavare il valore di ogni iperparametro.

    if (classifiertype == 'rf'):
        # Estraggo gli iperparametri come stringhe a partire dal nome del file
        n_estimators, criterion, max_depth, max_features = maxfile.split('__')[4:-1]

        # Li casto al loro tipo corretto (N.B. "criterion" deve essere "str", quindi
        # non richiede casting; stesso discorso per "max_features", qualora fosse "sqrt")
        n_estimators = int(n_estimators)
        if max_depth == 'None':
            max_depth = None
        else:
            max_depth = int(max_depth)

        if max_features != 'sqrt':
            max_features = int(max_features)

        # Preparo la lista contenente gli iperparametri ottimali
        outputpar = [n_estimators, criterion, max_depth, max_features]

    elif (classifiertype == 'nn'):
        # Estraggo gli iperparametri come stringhe a partire dal nome del file
        n_neurons, activation, solver, alpha, learning_rate_init, max_iter = maxfile.split('__')[3:-1]

        # Li casto al loro tipo corretto (N.B. "n_neurons" va converito in un np.array,
        # quindi il casting non è diretto; "activation" e "solver" sono "str" quindi non
        # richiedono casting)
        n_neurons = int(n_neurons)
        alpha = float(alpha)
        learning_rate_init = float(learning_rate_init)
        max_iter = int(max_iter)

        # Preparo il dizionario contenente gli iperparametri
        outputpar = [n_neurons, activation, solver, alpha, learning_rate_init, max_iter]
    return(outputpar)

# ==============================================================================
#                            OPTIMAL HYPERPARAMETERS
# ==============================================================================
# Determino gli iper-parametri del classificatore ottimizzato
print(f"Locating the optimal point of work...")
all_outfiles = glob.glob(os.path.join(temploc,f'gsout__{beamtype}__{crystalcond}__{classifiertype}__*__done.npz'))
all_accuracies = np.zeros((len(all_outfiles),))

for i,el in enumerate(all_outfiles):
    # Carico il file i-esimo e ne estraggo l'accuracy
    with np.load(el) as file:
        all_accuracies[i] = file['av_classifier_accuracy']

# Determino il punto a massima accuracy
maxind = np.argmax(all_accuracies)
maxfile = all_outfiles[maxind]

# Determino i corrispondenti iperparametri, sapendo come il file è codificato.
# N.B. la codifica è ovviamente diversa in base al tipo di classificatore
gs_bestpar = OutputCaster(maxfile,classifiertype)
print(f"Done! \n")

# ==============================================================================
#                        SEMI-OPTIMAL HYPERPARAMETERS: 1D
# ==============================================================================
# Voglio vedere la variazione dell'accuracy durante la Grid Search, fissando
# il valore di tutti gli iperparametri al loro valore ottimale e considerando
# la variazione di uno degli altri

# Costruisco la lista contenente tutti gli iper-parametri ottimali e una * al posto
# di quelli che voglio fissare
gs_thispar = copy.copy(gs_bestpar)
gs_thispar[parind_1] = '*'

# Costruisco la stringa che contiene il nome di tutti i file da considerare
tempoutprename = f"gsout__{beamtype}__{crystalcond}__{classifiertype}"

if (classifiertype == 'rf'):
    tempoutmidname = f"__{gs_thispar[0]}__{gs_thispar[1]}__{gs_thispar[2]}__{gs_thispar[3]}__"
elif (classifiertype == 'nn'):
    tempoutmidname = f"__{gs_thispar[0]}__{gs_thispar[1]}__{gs_thispar[2]}__{gs_thispar[3]}__{gs_thispar[4]}__{gs_thispar[5]}__"

tempoutpostname = "done.npz"
tempoutfullname = os.path.join(temploc,tempoutprename+tempoutmidname+tempoutpostname)
alloutfullname = glob.glob(tempoutfullname)

# Apro tutti i file che soddisfano la forma qui indicata
all_par1values = np.zeros((len(alloutfullname),),dtype=object)
all_acc1values = np.zeros((len(alloutfullname),),dtype=np.float64)
all_acc1errs = np.zeros_like(all_acc1values)

for i,el in enumerate(alloutfullname):
    # Estraggo il valore del parametro che sto variando e la corrispondente accuracy
    gs_semibestpar = OutputCaster(el,classifiertype)
    all_par1values[i] = gs_semibestpar[parind_1]
    with np.load(el) as file:
        all_acc1values[i] = file['av_classifier_accuracy']
        all_acc1errs[i] = file['std_classifier_accuracy']

# Pulisco i parametri, convertendo alcuni valori str o None in valori numerici convenzionali
all_par1values[(all_par1values == 'gini')] = 0
all_par1values[(all_par1values == 'entropy')] = 1
all_par1values[(all_par1values == 'sqrt')] = 10
all_par1values[(all_par1values == None)] = 0
all_par1values = np.array(all_par1values,dtype=np.float64)

# Ordino i punti
sortind = np.argsort(all_par1values)
all_par1values = all_par1values[sortind]
all_acc1values = all_acc1values[sortind]
all_acc1errs = all_acc1errs[sortind]

# Plotto la mappa: valore di par1 contro accuracy
all_labelstr = [f"{conversion_list[i]} = {gs_thispar[i]} \n" for i in(range(len(conversion_list)))]
labelstr = ""
for i,el in enumerate(all_labelstr):
    labelstr += el

fig, ax = plt.subplots(figsize = dimfig)
ax.errorbar(all_par1values,100*all_acc1values,100*all_acc1errs,
            color = 'mediumblue', linestyle = '-', linewidth = 1,
            marker = '.', markersize = 5, capsize = 5,
            label = labelstr[:-2])

ax.set_xlabel(f'{conversion_list[parind_1]}',fontsize = textfont)
ax.set_ylabel('K-averaged accuracy [%]',fontsize = textfont)
ax.grid(linewidth=0.5)
ax.tick_params(labelsize = textfont)
ax.legend(fontsize=textfont, loc = 'best',framealpha=1)
fig.set_tight_layout('tight')
figfilename = f'{classifiertype}_gsresults1D_{crystalcond}_par_{parind_1}'+filetype
fig.savefig(os.path.join(imgloc,f"{beamtype}",figfilename),dpi=filedpi)
plt.close(fig)

print(f"1D curve plot: done!")

# ==============================================================================
#                        SEMI-OPTIMAL HYPERPARAMETERS: 2D
# ==============================================================================
# Voglio vedere la variazione dell'accuracy durante la Grid Search, fissando
# il valore di tutti gli iperparametri al loro valore ottimale e considerando
# la variazione di due degli altri

# Costruisco la lista contenente tutti gli iper-parametri ottimali e una * al posto
# di quelli che voglio fissare
gs_thispar = copy.copy(gs_bestpar)
gs_thispar[parind_1] = '*'
gs_thispar[parind_2] = '*'

# Costruisco la stringa che contiene il nome di tutti i file da considerare
tempoutprename = f"gsout__{beamtype}__{crystalcond}__{classifiertype}"

if (classifiertype == 'rf'):
    tempoutmidname = f"__{gs_thispar[0]}__{gs_thispar[1]}__{gs_thispar[2]}__{gs_thispar[3]}__"
elif (classifiertype == 'nn'):
    tempoutmidname = f"__{gs_thispar[0]}__{gs_thispar[1]}__{gs_thispar[2]}__{gs_thispar[3]}__{gs_thispar[4]}__{gs_thispar[5]}__"

tempoutpostname = "done.npz"
tempoutfullname = os.path.join(temploc,tempoutprename+tempoutmidname+tempoutpostname)
alloutfullname = glob.glob(tempoutfullname)

# Apro tutti i file che soddisfano la forma qui indicata
all_par1values = np.zeros((len(alloutfullname),),dtype=object)
all_par2values = np.zeros((len(alloutfullname),),dtype=object)
all_accvalues = np.zeros((len(alloutfullname),),dtype=np.float64)

for i,el in enumerate(alloutfullname):
    # Estraggo il valore del parametro che sto variando e la corrispondente accuracy
    gs_semibestpar = OutputCaster(el,classifiertype)
    all_par1values[i] = gs_semibestpar[parind_1]
    all_par2values[i] = gs_semibestpar[parind_2]
    with np.load(el) as file:
        all_accvalues[i] = file['av_classifier_accuracy']
        
# Pulisco i parametri, convertendo alcuni valori str o None in valori numerici convenzionali
all_par1values[(all_par1values == 'gini')] = 0
all_par1values[(all_par1values == 'sqrt')] = 10
all_par1values[(all_par1values == None)] = 0
all_par1values = np.array(all_par1values,dtype=np.float64)

all_par2values[(all_par2values == 'gini')] = 0
all_par2values[(all_par2values == 'sqrt')] = 10
all_par2values[(all_par2values == None)] = 0
all_par2values = np.array(all_par2values,dtype=np.float64)

# Determino i valori disponibili per i due parametri in variazione
uni_par1values = np.unique(all_par1values)
uni_par2values = np.unique(all_par2values)

# Calcolo come varia l'accuracy in due dimensioni
histo2D = np.zeros((np.shape(uni_par1values)[0],np.shape(uni_par2values)[0]))
for i,eli in enumerate(uni_par1values):
    for j,elj in enumerate(uni_par2values):
        cond = (all_par1values == eli) & (all_par2values == elj)
        histo2D[i,j] = all_accvalues[cond]

# Lo mostro
all_labelstr = [f"{conversion_list[i]} = {gs_thispar[i]}, " for i in(range(len(conversion_list)))]
titlestr = ""
for i,el in enumerate(all_labelstr):
    if (i != parind_1) & (i != parind_2):
        titlestr += el

fig, ax = plt.subplots(figsize = dimfig)
histoplotter2D(ax,uni_par1values,uni_par2values,100*histo2D,'jet',100*np.mean(histo2D),
               f'{conversion_list[parind_1]}',f'{conversion_list[parind_2]}',
               textfont,False,ifcbarfont=True)

ax.set_title(titlestr[:-2],fontsize=textfont)
fig.set_tight_layout('tight')
figfilename = f'{classifiertype}_gsresults2D_{crystalcond}_par_{parind_1}_{parind_2}'+filetype
fig.savefig(os.path.join(imgloc,f"{beamtype}",figfilename),dpi=filedpi)
plt.close(fig)

print(f"2D curve plot: done!")
print(f"Job done!\n")
print(f"*"*30 + '\n')