# Measurement of the Particle Identification capability of the OREO calorimeter

Author and maintainer: [P. Monti-Guarnieri](mailto:monti.guarnieri.p@gmail.com)

# Content
## List of the scripts

This code is used to analyze the data produced by a simulation of a prototype OREO calorimeter (see [here](https://gitlab.com/piemmegi/proto-oriented-sac)), which is essentially a prototype Small Angle Calorimeter for the HIKE/KLEVER experiment. The simulation implements a simple setup (composed essentially of only the active crystals which compose the calorimeter, without actually modelling the detection planes or the inter-tower gaps) and aims at testing if a mixed layout (== oriented + non oriented crystals) is actually capable to provide a better Particle Identification capability, rather than an homogeneous layout composed of only randomly aligned crystals. The features used for the analysis are simply the energies deposited in each crystal, the fractional energy deposited in each layer and the total energy deposited in the calorimeter. The code available in this repository is **used to analyze such data** and is structured as follows.

**Scripts for dataset preparation or preliminary plotting:**
- `dataplotter.py`: the script takes as input four simulation files ( (axial or random) + (gamma or neutron) ), produced with a given beam configuration and plots the spectrum of the total energy deposited in the calorimeter in each configuration. It is useful to check if the total generated statistics is enough for the subsequent analyses.
- `datapreparator.py`: the script takes as input four simulation files (as above) and uses them to extract the dataset for the training of the ML algorithms.

**Scripts for PID (manual or with Machine Learning):**
- `classifier_gridseach.py`: the script performs a Grid Search on a given set of hyperparameters for a given classifier (at the moment, either Random Forest or Neural Network), trained and tested on the dataset preparated by `datapreparator.py` (either axial or random). At each step of the Grid Search, a K-Fold validation is performed and the corresponding accuracy is saved in a dedicated file.
- `classifier_gridseachreport.py`: the script shows how the classification accuracy varies as a function of one or two hyperparameters in the Grid Search (while keeping all the other hyperparameters fixed to their optimal values).
- `classifier_test.py`: the script performs a K-Fold Cross-validation with a given classifier, optimized previously with the other scripts. In the end, several scores are saved, for a subsequent analysis. The most important ones are:
	1. The K-Fold averaged accuracy.
	2. The K-Fold averaged ROC curve.
	3. The K-Fold averaged Precision - Recall (or Purity - Efficiency) curve.
- `classifier_finalreport.py`: the script summarizes the results obtained by the previous steps in a couple of plots (accuracy curves, ROC curves).
- `classifier_finalreport_PR.py`: the script summarizes the results obtained by the previous steps in a couple of plots (precision curves, recall curves, Precision-Recall curves).
- `classifier_mastercall.py`: the script calls the entire Machine Learning classifiation pipeline, in order (i.e., all the ones described until here).

**Scripts for other aesthetic plots:**
- `pubplots_manualclass.py`: the script takes as input four simulation files (as above) and uses them to produce some plots, useful to understand how the manual classification works. In particular, it shows the scatterplot between two among three of the following quantities: E$_{tot}$, E$_{L,0}$, E$_{L,1}$ (with the last two either normalized or not)	
- `pubplots_shower3D.py`: the script takes as input four simulation files (as above, but here produced with thinner crystalline layers) and uses them to produce a 3D rendering of the development of the photon and neutron shower in the medium.

## Available figures
- From `datapreparator`:
	1. Distribution of $E_{tot}$ for random and axial, photon and gamma, after the selection cut.
	2. Sorting of the classes in the datasets.
- From `dataplotter`:
	1. Distribution of $E_{tot}$ for random and axial, photon and gamma, after the selection cut.
- From `pubplots_shower3D`:
	1. 3D development of the e.m. shower in a calorimeter with more towers than normal.
- From `pubplots_manualclass`:
	1. Scatterplot: E$_{tot}$ vs E$_{L,0}$
	2. Scatterplot: E$_{tot}$ vs E$_{L,0}$ (normalized to E$_{tot}$)
	3. Scatterplot: E$_{tot}$ vs E$_{L,1}$
	4. Scatterplot: E$_{tot}$ vs E$_{L,1}$ (normalized to E$_{tot}$)
	5. Scatterplot: E$_{L,0}$ vs E$_{L,1}$
	6. Scatterplot: E$_{L,0}$ vs E$_{L,1}$ (both normalized to E$_{tot}$)
- From `classifier_gridsearchreport`:
	1. Accuracy vs value of one hyperparameter in the Grid Search
	2. Accuracy vs value of two hyperparameters in the Grid Search (intensity map)
- From `classifier_finalreport.py`:
	1. Accuracy vs energy deposited (+ diff)
	2. ROC curves at 5 fixed energies (+ diff + histo2D of the diff)
- From `classifier_finalreport_PR.py`:
	1. Precision vs energy deposited (+ diff)
	2. Recall vs energy deposited (+ diff)
	3. PR curves at 5 fixed energies (+ diff + histo2D of the diff)
	
# Experimental setup

Layout of the calorimeter:

![Layout of the OREO calorimeter](Documentation/calo_scheme.png "Layout of the OREO calorimeter"){width=75%}

In this scheme, each coloured box is a PbWO$_4$ crystal, with a transverse area of 1 cm $\times$ 1 cm and a length of 4 cm ($\sim 4.5$ X$_0$). The incident beams feature the following parameters, in both axial and amorphous mode:

| Beam particle | Spatial profile          | Angular profile                   | Energy spectrum       | 
|:-------------:|:------------------------:|:---------------------------------:|:---------------------:|
| Neutron       | Square (FWHM = $3.5$ cm) | 2D-gaussian ($\sigma = 0.1$ mrad) | Uniform ($1-251$ GeV) | 
| $\gamma$      | Square (FWHM = $3.5$ cm) | 2D-gaussian ($\sigma = 0.1$ mrad) | Uniform ($1-501$ GeV) |

# Quick commands and memo

On Windows (inside a conda environment called `pypmg`)
```bash
cd "C:\Users\Pietro\Desktop\Academic Research\Analisi dati\Gitworks\proto-oriented-sac_analysis"
conda activate pypmg
python datapreparator.py
python randomforest_advancedmaster.py
```

# Some references

- Sklearn's guide on [random forests](https://scikit-learn.org/stable/modules/ensemble.html#forest).
- Managing [environments](https://conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html) in Conda.
