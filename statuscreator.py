# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è produrre un file di stato JSON che contiene alcune info utili
# per lo svolgimento degli altri script di analisi del gruppo proto-oriented-sac_analysis.
#
# Sintassi di call:
#   python statuscreator.py
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
import json, os

# ==========================================================================================
#                                  CODIFICA DEI FILE DATI
# ==========================================================================================
# Definisco se mi trovo in VM o no
ifVM = False

# Definisco se sono in PMG-PC1 (mobile LENOVO) o PMG-PC2 (fisso assemblato)
numPMGPC = 1

# Definisco il path dove i file dati si trovano
if ifVM:
    fileprename = '/eos/user/p/pmontigu/Datadirs/datadir_2023/G4_SAC/'
else:
    if numPMGPC > 1:
        fileprename = 'D:\\Academic Research\\Analisi dati\\Datadirs\\Insulab\\datadir_2023\\G4_SAC\\'
    else:
        fileprename = 'C:\\Users\\Pietro\\Desktop\\Academic Research\\Analisi dati\\Datadirs\\Insulab\\datadir_2023\\G4_SAC\\'
    
# Definisco il numero di core da usare nell'addestramento di classificatori intelligenti
n_jobs = -1

# Definisco il random state per l'inizializzazione degli algoritmi di Machine Learning
random_state = 0

# Definisco dove salvare le immagini prodotte.
imgloc = os.path.join('.','Images')

# Definisco dove salvare i file prodotti temporaneamente.
temploc = os.path.join('.','Tempfiles')

# ==========================================================================================
#                                  CREAZIONE DEL FILE JSON
# ==========================================================================================
# Creo la stringa da convertire in JSON
jsonstring = {"ifVM": ifVM,                   # Sono in VM (o in WSL)?
              "fileprename": fileprename,     # Path dove i file si trovano
              "n_jobs": n_jobs,               # Quanti core usare per il training dei classificatori
              "imgloc": imgloc,               # Dove salvare le immagini
              "temploc": temploc,             # Dove salvare i file temporanei
              "numPMGPC": numPMGPC,           # Numero di PMGPC su cui mi trovo
              "random_state": random_state}   # Random state per inizializzare la macchina

# Converto la stringa in JSON
json_string = json.dumps(jsonstring,indent=4)

# Scrivo il file
filename = os.path.join('.','statusfile.json')
with open(filename, 'w') as outfile:
    outfile.write(json_string)
    print(f"Status file written: ")
    print(filename)
    print(f"")
